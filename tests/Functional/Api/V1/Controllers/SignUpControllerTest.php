<?php

namespace App\Functional\Api\V1\Controllers;

use Config;
use App\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SignUpControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function testSignUpSuccessfully()
    {
        $this->post('api/user/signup', [
            'name' => 'Test User',
            'surname' => 'aaa',
            'dni' => '71230167C',
            'genre' => 'M',
            'email' => 'test@email.com',
            'phone' => '684034563',
            'password' => '123456',
            'password_confirmation' => '123456'
        ])->assertJson([
            'status' => 1
        ])->assertStatus(201);
    }


    public function testSignUpReturnsValidationError()
    {
        $this->post('api/user/signup', [
            'name' => 'Test User',
            'email' => 'test@email.com',
            'phone' => '684034563',
            'password' => '123456',
            'password_confirmation' => '1234565'
        ])->assertJsonStructure([
            'error'
        ])->assertStatus(422);
    }
}
