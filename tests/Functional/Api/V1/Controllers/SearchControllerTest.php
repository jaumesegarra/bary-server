<?php

namespace App\Functional\Api\V1\Controllers;

use Hash;
use Carbon\Carbon;
use App\Restaurant;
use App\RestaurantScheduler;
use App\RestaurantSchedulerDay;
use App\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SearchControllerTest extends TestCase
{
	use DatabaseTransactions;

	public function setUp()
	{
		parent::setUp();

		// CREATE SCHEDULER
		$sch = new RestaurantScheduler([
			'restaurant_id' => 17,
			'date_start' => new Carbon(),
			'descripcion' => 'current',
			'capacity' => 10,
			'average_time' => 30
		]);

		$sch->save();

		(new RestaurantSchedulerDay([
			'code' => 'aaaa',
			'scheduler_id' => $sch->id,
			'week_day' => (new Carbon())->dayOfWeek,
			'hour_start' => '01:00',
			'hour_end' => '23:30'
		]))->save();
	}

	public function testSuggestions()
	{
		$this->get('api/c/1/search/term/frances')->assertJsonCount(1, 'restaurants')->assertStatus(200)->isOk();
	}

	public function testSearch()
	{
		$this->get('api/c/1/search', [
			'q' => 'a'
		])->assertJsonCount(6, 'restaurants.data')->assertStatus(200)->isOk();
	}

	public function testSearchFilterAvailable()
	{	
		$date = Carbon::now()->addHour();
		$date->minute = 0;
		$date->second = 0;

		$this->get('api/c/1/search?q=a&date='.$date->toDateTimeString().'&num_pers=10')->assertJsonCount(1, 'restaurants.data')->assertStatus(200)->isOk();
	}	

	public function testSearchFilterAvailableNotResults()
	{	
		$date = Carbon::now()->addHour();
		$date->minute = 0;
		$date->second = 0;

		$this->get('api/c/1/search?q=a&date='.$date->toDateTimeString().'&num_pers=30')->assertJsonCount(0, 'restaurants.data')->assertStatus(200)->isOk();
	}
}