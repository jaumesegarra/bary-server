<?php

namespace App\Functional\Api\V1\Controllers;

use Hash;
use Carbon\Carbon;
use App\User;
use App\Restaurant;
use App\RestaurantScheduler;
use App\RestaurantSchedulerDay;
use App\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RestaurantControllerTest extends TestCase
{
    use DatabaseTransactions;

    private $user;
    private $restaurant;

    public function setUp()
    {
        parent::setUp();

        $this->user = new User([
            'name' => 'Test',
            'email' => 'test@email.com',
            'password' => '123456'
        ]);

        if(!$this->user->save())
            die('Error creating test examples');

        $this->restaurant = new Restaurant([
            'name' => 'The Grand Example',
            'address' => 'uknow',
            'phone' => '966666666',
            'city_id' => 1,
            'restaurant_type_id' => 1
        ]);

        $this->restaurant->approbed = 1;

        if(!$this->restaurant->save())
            die('Error creating test examples');
    }

    private function createScheduler(){
        $sch = new RestaurantScheduler([
            'restaurant_id' => $this->restaurant->id,
            'date_start' => new Carbon(),
            'descripcion' => 'current',
            'capacity' => 10,
            'average_time' => 30
        ]);

        $sch->save();

        $hour_start = Carbon::now()->addHour();
        $hour_start->minute = 0;
        $hour_start->second = 0;

        (new RestaurantSchedulerDay([
            'code' => 'aaaa',
            'scheduler_id' => $sch->id,
            'week_day' => (new Carbon())->dayOfWeek,
            'hour_start' => '01:00',
            'hour_end' => '23:30'
        ]))->save();
    }

    public function testIndexWithoutLoggin()
    {

        $this->get('api/restaurant/'.$this->restaurant->id, [])->assertJson([
            'name' =>  $this->restaurant->name,
            'type' => $this->restaurant->type->name,
            'was_client' => null,
            'is_favourite' =>  null,
            'available_days' => false
        ])->isOk();
    }

    public function testAddToFavourites()
    {

        $response = $this->post('api/user/login', [
            'email' => 'test@email.com',
            'password' => '123456'
        ]);

        $response->assertStatus(200);

        $responseJSON = json_decode($response->getContent(), true);
        $token = $responseJSON['token'];

        DB::table('user_restaurant_favourite')->insert([
            'restaurant_id' => $this->restaurant->id,
            'user_id' => $this->user->id
        ]);

        $this->get('api/restaurant/'.$this->restaurant->id, [
            'Authorization' => 'Bearer '.$token
        ])->assertJson([
            'name' =>  $this->restaurant->name,
            'type' => $this->restaurant->type->name,
            'was_client' => false,
            'is_favourite' =>  true,
            'available_days' => null
        ])->isOk();
    }

    public function testGetReserveAvDays(){
        $response = $this->post('api/user/login', [
            'email' => 'test@email.com',
            'password' => '123456'
        ]);

        $response->assertStatus(200);

        $responseJSON = json_decode($response->getContent(), true);
        $token = $responseJSON['token'];

        (new RestaurantScheduler([
            'restaurant_id' => $this->restaurant->id,
            'date_start' => new Carbon(),
            'descripcion' => 'current',
            'capacity' => 10,
            'average_time' => 60
        ]))->save();

        $this->get('api/restaurant/'.$this->restaurant->id, [
            'Authorization' => 'Bearer '.$token
        ])->assertJsonFragment([
            'available_days' => []
        ])->isOk();

    }

    public function testGetReserveAvHours(){
        $response = $this->post('api/user/login', [
            'email' => 'test@email.com',
            'password' => '123456'
        ]);

        $response->assertStatus(200);

        $responseJSON = json_decode($response->getContent(), true);
        $token = $responseJSON['token'];

        $this->createScheduler();

        $hour = Carbon::now()->addHour();
        $hour->minute = 0;
        $hour->second = 0;

        $this->get('api/restaurant/'.$this->restaurant->id.'/reserve/'.(new Carbon())->toDateString(), [
            'Authorization' => 'Bearer '.$token
        ])->assertJsonFragment([
            [
                'hour' => $hour->format('H:i'),
                'num_pers' => 10
            ]
        ])->isOk();
    }

    public function testReserve()
    {
        $response = $this->post('api/user/login', [
            'email' => 'test@email.com',
            'password' => '123456'
        ]);

        $response->assertStatus(200);

        $responseJSON = json_decode($response->getContent(), true);
        $token = $responseJSON['token'];

        $this->createScheduler();

        $this->post('api/restaurant/'.$this->restaurant->id.'/reserve/'.(new Carbon())->toDateString(), [
            'hour' => (new Carbon())->addMinutes(30)->format('H:i'),
            'num_pers' => 5,
            'suggestions' => 'aa'
        ], [
            'Authorization' => 'Bearer '.$token
        ])->assertJson([
            'status' =>  1
        ])->isOk();
    }

    public function testReserveDisallowedCapacity()
    {
        $response = $this->post('api/user/login', [
            'email' => 'test@email.com',
            'password' => '123456'
        ]);

        $response->assertStatus(200);

        $responseJSON = json_decode($response->getContent(), true);
        $token = $responseJSON['token'];

        $this->createScheduler();

        $this->post('api/restaurant/'.$this->restaurant->id.'/reserve/'.(new Carbon())->toDateString(), [
            'hour' => (new Carbon())->addHour()->format('H:i'),
            'num_pers' => 15,
            'suggestions' => 'aa'
        ], [
            'Authorization' => 'Bearer '.$token
        ])->assertStatus(422)->isOk();
    }
}
