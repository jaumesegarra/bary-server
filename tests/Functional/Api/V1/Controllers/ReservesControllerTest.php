<?php

namespace App\Functional\Api\V1\Controllers;

use Hash;
use Carbon\Carbon;
use App\User;
use App\UserReserve;
use App\Restaurant;
use App\RestaurantScheduler;
use App\RestaurantSchedulerDay;
use App\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ReservesControllerTest extends TestCase
{
	use DatabaseTransactions;

	private $user;
	private $editable;

	public function setUp()
	{
		parent::setUp();

        // CREATE USER
		$this->user = new User([
			'name' => 'Test',
			'email' => 'test@email.com',
			'password' => '123456'
		]);

		if(!$this->user->save())
			die('Error creating test examples');


        // CREATE SCHEDULER
		$sch = new RestaurantScheduler([
			'restaurant_id' => 17,
			'date_start' => new Carbon(),
			'descripcion' => 'current',
			'capacity' => 15,
			'average_time' => 30
		]);

		$sch->save();

		(new RestaurantSchedulerDay([
			'code' => 'aaaa',
			'scheduler_id' => $sch->id,
			'week_day' => (new Carbon())->dayOfWeek,
			'hour_start' => '01:00',
			'hour_end' => '23:30'
		]))->save();
		
		// CREATE RESERVE
		
		$date = Carbon::now()->addHour();

		if($date->minute >= 30)
			$date->minute = 30;
		else
			$date->minute = 0;

		$date->second = 0;

		$this->editable = new UserReserve([
			'code' => 'sdabduids',
			'user_id' => $this->user->id,
			'restaurant_id' => 17,
			'date' => $date,
			'num_pers' => 10
		]);

		$this->editable->save();


		(new UserReserve([
			'code' => 'dfsdfsdf',
			'user_id' => $this->user->id,
			'restaurant_id' => 17,
			'date' => $date,
			'num_pers' => 10,
			'canceled' => 1
		]))->save();
	}


	public function testShowReserves()
	{
		$response = $this->post('api/user/login', [
            'email' => 'test@email.com',
            'password' => '123456'
        ]);

        $response->assertStatus(200);

        $responseJSON = json_decode($response->getContent(), true);
        $token = $responseJSON['token'];


        $this->get('api/user/reserves', [
            'Authorization' => 'Bearer '.$token
        ])->assertJsonCount(1, 'coming')->assertJsonCount(1, 'others.data')->isOk();
	}

	public function testEditReserve()
	{
		$response = $this->post('api/user/login', [
            'email' => 'test@email.com',
            'password' => '123456'
        ]);

        $response->assertStatus(200);

        $responseJSON = json_decode($response->getContent(), true);
        $token = $responseJSON['token'];


        $this->post('api/user/reserves/'.$this->editable->code.'/edit', [
        	'num_pers_additional' => 5
        ], [
            'Authorization' => 'Bearer '.$token
        ])->assertJson(['status' => 1])->isOk();
	}

	public function testEditReserveFail()
	{
		$response = $this->post('api/user/login', [
            'email' => 'test@email.com',
            'password' => '123456'
        ]);

        $response->assertStatus(200);

        $responseJSON = json_decode($response->getContent(), true);
        $token = $responseJSON['token'];


        $this->post('api/user/reserves/'.$this->editable->code.'/edit', [
        	'num_pers_additional' => 15
        ], [
            'Authorization' => 'Bearer '.$token
        ])->assertJsonStructure(['error'])->assertStatus(422)->isOk();
	}

	public function testCancelReserve()
	{
		$response = $this->post('api/user/login', [
            'email' => 'test@email.com',
            'password' => '123456'
        ]);

        $response->assertStatus(200);

        $responseJSON = json_decode($response->getContent(), true);
        $token = $responseJSON['token'];


        $this->post('api/user/reserves/'.$this->editable->code.'/cancel', [], [
            'Authorization' => 'Bearer '.$token
        ])->assertJson(['status' => 1])->assertStatus(200)->isOk();
	}
}
