<?php

return [
	'route_path' => '/uploads/',
	'full_size' => 'r_photos/original/',
	'icon_size' => 'r_photos/thumb/',
];