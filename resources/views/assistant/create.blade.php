@extends('layouts.manage')

@section('content')
@component('components.portrait')
Tu Restaurante
@endcomponent
<section class="background-white padding-percent">
    {{ Form::open(['class' => 'form max-medium', 'route' => 'assistant.create.post', 'method' => 'POST']) }}
    <h3>Datos del restaurante</h3>
    <p class="info">Ahora solo falta que proporciones la información de tu restaurante (Cuanto más pongas más repercusión en Bary tendrá):</p>

    <div class="columns col2 colm1">
        <div class="form-field">
            <label>Nombre
                <input type="text" class="{{ $errors->has('name') ? 'error' : '' }}" name="name" value="{{ old('name') }}" required autofocus/>
            </label>

            @if ($errors->has('name'))
            <span class="msg error">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
            @endif
        </div>
        <div class="form-field">
            <label>Tipo
                <select name="type_id" required="required" value="{{ old('type_id') }}">
                    <option value="">Select one...</option>

                    @foreach($restaurant_types as $restaurant_type)
                    <option value="{{$restaurant_type->id}}">{{$restaurant_type->name}}</option>
                    @endforeach
                </select>
            </label>

            @if ($errors->has('type_id'))
            <span class="msg error">
                <strong>{{ $errors->first('type_id') }}</strong>
            </span>
            @endif
        </div>
        <div class="form-field">
            <label>Ciudad
                <select name="city_id" required="required" value="{{ old('city_id') }}">
                    <option value="">Select one...</option>

                    @foreach($cities as $city)
                    <option value="{{$city->id}}">{{$city->name}}, {{$city->country}}</option>
                    @endforeach
                </select>
            </label>

            @if ($errors->has('city_id'))
            <span class="msg error">
                <strong>{{ $errors->first('city_id') }}</strong>
            </span>
            @endif
        </div>
        <div class="form-field">
            <label>Teléfono
                <input type="text" name="phone" required="required" value="{{ old('phone') }}"/>
            </label>

            @if ($errors->has('phone'))
            <span class="msg error">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
            @endif
        </div>
        <div class="form-field">
            <label>Dirección
                <textarea name="address" required="required">{{ old('address') }}</textarea>
            </label>

            @if ($errors->has('address'))
            <span class="msg error">
                <strong>{{ $errors->first('address') }}</strong>
            </span>
            @endif
        </div>
        <div class="form-field">
            <label>Descripción
                <textarea name="description" required="required">{{ old('description') }}</textarea>
            </label>

            @if ($errors->has('description'))
            <span class="msg error">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
            @endif
        </div>
        <div class="form-field">
            <label>Etiquetas (máximo 10) <span class="info">[Separadas por comas]</span>
                <textarea name="tags" class="{{ $errors->has('tags') ? 'error' : '' }}">{{ old('tags') }}</textarea>
            </label>

            @if ($errors->has('tags'))
            <span class="msg error">
                <strong>{{ $errors->first('tags') }}</strong>
            </span>
            @endif
        </div>

        <script type="text/javascript">
            window.onload = function() {
                $("select[name='city_id']").combobox();
                $("select[name='type_id']").combobox();

                function split( val ) {
                    return val.split( /,\s*/ );
                }

                function extractLast( term ) {
                    return split( term ).pop();
                }

                $("textarea[name='tags']")
                .on( "keydown", function(event) {
                    if(event.keyCode === $.ui.keyCode.TAB && $(this).autocomplete( "instance" ).menu.active ) {
                        event.preventDefault();
                    }
                })
                .autocomplete({
                    source: function(request, response) {
                        $.getJSON("{{route('assistant.ajax.restaurant_tags')}}", {
                            term: extractLast( request.term )
                        }, response );
                    },
                    search: function() {
                        var term = extractLast( this.value );
                        if (term.length < 1) {
                            return false;
                        }
                    },
                    focus: function() {
                        return false;
                    },
                    select: function(event, ui){
                        var terms = split( this.value );
                        terms.pop();
                        terms.push( ui.item.value );
                        terms.push( "" );
                        this.value = terms.join( ", " );
                        return false;
                    }
                });
            };
        </script>
    </div>

    <div class="foot">
        {{ link_to_route('home', $title = 'Omitir por ahora', $parameters = [], $attributes = ['class' => 'small']) }}

        <button type="submit" class="button success">
            Siguiente
        </button>
    </div>
    {{ Form::close() }}
</section>

@endsection
