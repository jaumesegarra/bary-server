@extends('layouts.mini')

@section('content')
<h2 class="error">Error 404</h2>
<p style="line-height: 1.5em;">Parece ser que esta página no existe todavía o ha sido eliminada, <br/> compruebe que haya introducido la dirección web correcta. </p>
<a href="javascript:window.history.go(-1)" class="button grey">Volver atrás</a>
@endsection