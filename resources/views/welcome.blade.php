@extends('layouts.app')

@section('content')
<div id="presentation">
	<div class="title">
		<div class="center">
			<h1>Controle su restaurante!</h1>
			<p>Con <i>Bary</i> podrá conocer a sus clientes, atraer a más gente, administrar sus reservas...</p>
		</div>
	</div>
	<div class="background"></div>
</div>
<div class="cards center">
	<div class="card green">
		<div class="card-content">
			<i class="la la-users"></i>
			<h4>Llegue a más gente</h4>
			<p>Gracias a nuestro sitio web y su sistema de búsqueda su restaurante estará a la vista de todos.</p>
		</div>
	</div>
	<div class="card purple">
		<div class="card-content">
			<i class="la la-calendar"></i>
			<h4>Administre sus reservas</h4>
			<p>Podrá estar al tanto de todas sus reservas registradas desde nuestra web, además de poder introducir nuevas manualmente.</p>
		</div>
	</div>
	<div class="card red">
		<div class="card-content">
			<i class="la la-bar-chart"></i>
			<h4>Estadísticas de su negocio</h4>
			<p>Conozca la popularidad de su restaurante, los días que más se reserva, etc...</p>
		</div>
	</div>
</div>
<main>
	<section>
		<h2 text="Proyecto en crecimiento">En crecimiento</h2>
		<p>
			Nuestro proyecto está en el inicio de algo grande, pero necesitamos a más restaurantes como el suyo involucrados en él para poder expandirnos más. Estas són algunas de las ciudades donde Bary está disponible al público:
		</p>
		<div class="card-grid">
			@foreach($cities as $city)
			@php ($photo = $city->random_image())
			<div class="card">
				<div class="card-thumbnail {{ ($photo != null) ? 'image-exists' : ''" style="background-image: url({!!$photo!!});">
					<div class="card-thumbnail-title">
						<h5>Valencia <font>ES</font></h5>
						<p>10 restaurantes</p>
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</section>

	<section>
		<h2 text="Completamente gratuito">Gratuito</h2>
		<p>
			Bary es totalmente gratuito para todos los usuarios, su modelo de negocio se basará en la publicidad incrustada en su sitio web.
		</p>
	</section>
</main>
<footer class="center">
	<span>© 2018 Bary, Todos los derechos reservados</span>
	<span>Creado por Jaume Segarra</span>
</footer>
@push('styles')
<link rel="stylesheet" type="text/css" href="/manager/css/presentation.css"/>
<link rel="stylesheet" type="text/css" href="/manager/css/welcome-page.css"/>
@endpush
@endsection
