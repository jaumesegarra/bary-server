@extends('layouts.manage')

@section('content')
@php ($role = Auth::guard()->user()->hasRoleAt($restaurant->id))

<h2>
	<font class="ellipsis">{{$restaurant->name}} <b style="font-size: 10px; color:#777">({{$restaurant->city->name}})</b></font>
	
	@if($restaurant->approbed == 0 && $role == 'A')
	<div class="right">
		<button class="button small aprove_request_js">Solicitar aprobación</button>
		{{ Form::open(['class' => 'aprove_request_form_js', 'route' => ['restaurant.approve_request', $restaurant->id], 'method' => 'POST']) }}
		{{ Form::close() }}
	</div>
	@endif
</h2>

<section class="background-white">
	<div class="content-panel">
		<ul class="nav">
			<a href="#" class="showMenu"><i class="la la-bars"></i></a>
			<li @if(Route::current()->getName() == 'restaurant.index') class="active" @endif><a href="{{route('restaurant.index',$restaurant->id)}}"><i class="la la-home"></i> Inicio</a></li>
			<li @if(Route::current()->getName() == 'restaurant.reserves.index') class="active" @endif><a href="{{route('restaurant.reserves.index',$restaurant->id)}}"><i class="la la-list-ul"></i> Reservas</a></li>
			<li @if(Route::current()->getName() == 'restaurant.menu.index') class="active" @endif><a href="{{route('restaurant.menu.index',$restaurant->id)}}"><i class="la la-cutlery"></i> Menú</a></li>
			<li @if(Route::current()->getName() == 'restaurant.photos.index') class="active" @endif><a href="{{route('restaurant.photos.index',$restaurant->id)}}"><i class="la la-camera-retro"></i> Fotos</a></li>
			<li @if(Route::current()->getName() == 'restaurant.schedulers.index') class="active" @endif><a href="{{route('restaurant.schedulers.index',$restaurant->id)}}"><i class="la la-calendar-o"></i> Horarios</a></li>
			@if(in_array($role, ['A', 'S']))
			<li @if(Route::current()->getName() == 'restaurant.statistics') class="active" @endif><a href="{{route('restaurant.statistics',$restaurant->id)}}"><i class="la la-area-chart"></i> Estadísticas</a></li>
			@endif
			@if($role == 'A')
			<li @if(strpos(Route::current()->getName(), 'restaurant.settings') !== false) class="active" @endif><a href="{{route('restaurant.settings.info',$restaurant->id)}}"><i class="la la-cog"></i> Configuración</a></li>
			@endif
		</ul>
		<div class="content">
			@if($restaurant->approbed == 0)
			<div class="msg info">
				Este restaurante aún no ha sido aprobado y por lo tanto no es visible para el resto de usuarios.
			</div>
			@endif
			@if($restaurant->approbed == 2)
			<div class="msg info">
				Este restaurante está a la espera de ser aprobado.
			</div>
			@endif
			@if($restaurant->hasPendingChanges())
			<div class="msg warn">
				Este restaurante está a la espera de que se apruebe su nueva información.
			</div>
			@endif

			@yield('main_content')
		</div>
	</div>
</section>
@push('styles')
<link rel="stylesheet" href="/manager/css/vex.css" />
<link rel="stylesheet" href="/manager/css/vex-theme-top.css" />
@endpush
@push('scripts')
<script src="/manager/js/vex.combined.min.js"></script>
<script>vex.defaultOptions.className = 'vex-theme-top'</script>
@endpush
@endsection