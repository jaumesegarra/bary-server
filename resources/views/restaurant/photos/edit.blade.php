@extends('layouts.restaurant')

@section('main_content')
<h4>Editar foto:</h4>
<div class="image-edit">
	<div class="img" style="background-image: url({!! $photo->getThumbUrl() !!})"></div>
	{{ Form::open(['class' => 'form', 'route' => ['restaurant.photos.update', $restaurant->id, $photo->path], 'method' => 'PUT']) }}
		<div class="form-field">
            <label>
            	<span>Descripción</span>
                <textarea class="{{ $errors->has('description') ? 'error' : '' }}" name="description" required autofocus>{{ (old('description')) ? old('description') : $photo->description }}</textarea>
            </label>

            @if ($errors->has('description'))
            <span class="msg error">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
            @endif
        </div>
        <div class="form-field">
            <label> 
            	<input type="radio" name="main" {{ ((old('main')) ? old('main') : $photo->isMain()) === true ? 'checked' : '' }} />
            	Imagen principal
            </label>
        </div>
        <div class="foot">
        	<button class="button small grey" type="reset">Resetear</button>
        	<button class="button small" type="submit">Guardar</button>
        </div>
	{{ Form::close() }}
</div>
@push('styles')
<link rel="stylesheet" type="text/css" href="/manager/css/photo-edit.css"/>
@endpush
@endsection