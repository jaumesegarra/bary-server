@extends('layouts.restaurant')

@section('main_content')
<h4>Subir imágenes</h4>
<script type="text/template" id="qq-template-manual-trigger">
	<div class="qq-uploader-selector qq-uploader">
		<div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
			<span class="qq-upload-drop-area-text-selector"></span>
		</div>
		<div class="buttons">
			<div class="qq-upload-button-selector button white">
				Seleccionar
			</div>
			<button type="button" id="trigger-upload" class="button">
				<i class="icon-upload icon-white"></i> Subir
			</button>
		</div>
		<span class="qq-drop-processing-selector qq-drop-processing">
			<span>Processing dropped files...</span>
			<span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
		</span>
		<ul class="qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals">
			<li>
				<div class="qq-thumb">
					<div class="qq-upload-spinner-selector">
						<div>
							<i class="la la-spinner"></i>
						</div>
					</div>
					<img class="qq-thumbnail-selector" qq-max-size="100" qq-server-scale>
				</div>

				<div class="qq-description">
					<input type="text" name="description" placeholder="Descripción" required="required">
					<span class="qq-upload-file-selector qq-upload-file ellipsis"></span>
				</div>

				<div class="qq-actions">
					<span class="qq-upload-size-selector qq-upload-size"></span>
					<button type="button" class="qq-upload-retry-selector button small grey">Retry</button>
					<a href="#" class="qq-upload-cancel-selector"><i class="la la-close"></i></a>
				</div>
			</li>
		</ul>

		<dialog class="qq-alert-dialog-selector">
			<div class="qq-dialog-message-selector"></div>
			<div class="qq-dialog-buttons">
				<button type="button" class="qq-cancel-button-selector">Close</button>
			</div>
		</dialog>

		<dialog class="qq-confirm-dialog-selector">
			<div class="qq-dialog-message-selector"></div>
			<div class="qq-dialog-buttons">
				<button type="button" class="qq-cancel-button-selector">No</button>
				<button type="button" class="qq-ok-button-selector">Yes</button>
			</div>
		</dialog>

		<dialog class="qq-prompt-dialog-selector">
			<div class="qq-dialog-message-selector"></div>
			<input type="text">
			<div class="qq-dialog-buttons">
				<button type="button" class="qq-cancel-button-selector">Cancel</button>
				<button type="button" class="qq-ok-button-selector">Ok</button>
			</div>
		</dialog>
	</div>
</script>

<input type="hidden" name="_endpoint" value="{!! route('restaurant.photos.store', $restaurant->id) !!}"/>
<input type="hidden" name="_csrf" value="{!! csrf_token() !!}"/>
<div id="fine-uploader-manual-trigger"></div>

@push('styles')
<link rel="stylesheet" type="text/css" href="/manager/css/fine-uploader-new.min.css"/>
@endpush
@push('scripts')
<script type="text/javascript" src="/manager/js/jquery.fine-uploader.min.js"></script>
<script type="text/javascript" src="/manager/js/photo_uploader.js"></script>
@endpush

@endsection