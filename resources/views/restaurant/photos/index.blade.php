@extends('layouts.restaurant')

@section('main_content')
@php ($role = Auth::guard()->user()->hasRoleAt($restaurant->id))

@if( $role == 'A' )
<h4>
	<!--Fotos:-->
	<div class="right">
		<a href="{{route('restaurant.photos.create', [$restaurant->id])}}" class="button small">Subir</a>
	</div>
</h4>
@endif
@if(count($restaurant->photos) > 0)
<div class="image-grid">
	@foreach($restaurant->photos as $photo)
	<div class="card" data-path="{{$photo->path}}">
		<div class="card-thumbnail image-exists" style="background-image: url('{{ $photo->getThumbUrl()}}');"></div>
		<div class="card-content">
			<p>{{ $photo->description }}</p>
			<div class="foot">
				@if( $photo->isMain())
				<span class="tag">PORTADA</span>
				@endif
				@if( $role == 'A' )
				<div class="right">
					<a href="{{ route('restaurant.photos.show', [$restaurant->id, $photo->path]) }}">
						<i class="la la-pencil"></i>
					</a>
					<a href="#" class="delete_js">
						<i class="la la-trash"></i>
					</a>
					{{ Form::open(['class' => 'destroy_sph', 'route' => ['restaurant.photos.destroy', $restaurant->id, $photo->path], 'method' => 'DELETE']) }}
					{{ Form::close() }}
				</div>
				@endif
			</div>
		</div>
	</div>
	@endforeach
</div>
@else
<p class="no-found">No ha subido todavía ninguna foto.</p>
@endif

@push('styles')
<link rel="stylesheet" type="text/css" href="/manager/css/photo-list.css"/>
@endpush
@push('scripts')
<script type="text/javascript" src="/manager/js/photo-list.js"></script>
@endpush
@endsection