@extends('layouts.restaurant')

@section('main_content')
@php ($role = Auth::guard()->user()->hasRoleAt($restaurant->id))

<h4>Listado de horarios:</h4>
@if(count($restaurant->schedulers) > 0)
<ul class="table">
	@foreach($restaurant->schedulers as $scheduler)
	<li>
		<div>
			<span class="date">{{ $scheduler->date_start->format('j/m/Y') }} - {{ ($scheduler->date_end) ? $scheduler->date_end->format('j/m/Y') : '∞' }}</span>
			<div class="ellipsis">{{$scheduler->description}}</div>
		</div>
		<div class="right actions">
			<a href="{{route('restaurant.schedulers.organize', [$restaurant->id, $scheduler->id])}}"><i class="la la-calendar"></i></a>
			@if($role == 'A')
			<a href="{{route('restaurant.schedulers.edit', [$restaurant->id, $scheduler->id])}}"><i class="la la-pencil"></i></a>
			<a href="#" class="remove_schedule_js"><i class="la la-trash"></i></a>
			{{ Form::open(['class' => 'destroy_sch', 'route' => ['restaurant.schedulers.destroy', $restaurant->id, $scheduler->id], 'method' => 'DELETE']) }}
			{{ Form::close() }}
			@endif
		</div>
	</li>
	@endforeach
</ul>
@else
<p>No se ha creado todavía ningún horario todavía, sus clientes no podrán reservar ni conocer el horario del local.</p>
@endif
@if($role == 'A')
<a href="{{route('restaurant.schedulers.create', $restaurant->id)}}" class="create_button_div">
	<i class="la la-plus"></i>
	<font>Crear nuevo</font>
</a>
@endif
<br/>
<h4>Dias festivos:</h4>
<div class="tags">
	@if($role == 'A')
	<div class="tag">
		{{ Form::open(['route' => ['restaurant.schedulers.closed_day.store', $restaurant->id], 'method' => 'POST']) }}
		<input type="text" class="{{ $errors->has('date') ? 'error' : '' }}" placeholder="dd/mm/YYYY" name="date" required="required" />
		<button type="submit">
			<i class="la la-angle-right"></i>
		</button>
		{{ Form::close() }}
	</div>
	@endif
	@foreach($restaurant->closed_days as $closed_day)
	<div class="tag">
		{{$closed_day->date->format('j/m/Y')}}
		@if($role == 'A')
		<a href="#" class="remove_closedday_js"><i class="la la-close"></i></a>
		{{ Form::open(['class' => 'destroy_cl_day', 'route' => ['restaurant.schedulers.closed_day.destroy', $restaurant->id, $closed_day->date], 'method' => 'DELETE']) }}
		{{ Form::close() }}
		@endif
	</div>
	@endforeach
</div>

@push('styles')
@if($role == 'A')
<link rel="stylesheet" type="text/css" href="/manager/css/melon.datepicker.css"/>
@endif
@endpush
@push('scripts')
@if($role == 'A')
<script type="text/javascript" src="/manager/js/moment-with-locales.js"></script>
<script type="text/javascript">
	$("input[name='date']").datepicker({
		minDate: new Date(),
		beforeShow: function() {
			$(this).datepicker("widget").wrap('<div class="ll-skin-melon"></div>');
		},
		onClose: function() {
			$(this).datepicker("widget").hide();
			$(this).datepicker("widget").unwrap();
		}
	});

	$('.remove_schedule_js').on('click', function (e) {
		e.preventDefault();
		var $schedule_info = $(this).parent();

		var $form = $('form.destroy_sch', $schedule_info);
		var scheduler_description = $('div:first-child > .ellipsis', $schedule_info.parents('li')).text();

		vex.dialog.confirm({
			message: "¿Está seguro/a que desea eliminar el horario '"+scheduler_description+"'?",
			callback: function (value) {
				if (value) {
					$form.submit();
				}
			}
		});
	})


	$('.remove_closedday_js').on('click', function (e) {
		e.preventDefault();

		var $form = $('form.destroy_cl_day', $(this).parent());
		$form.submit();
	})
</script>
@endif
@endpush
@endsection
