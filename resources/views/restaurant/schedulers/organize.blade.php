@extends('layouts.restaurant')

@section('main_content')
@php ($role = Auth::guard()->user()->hasRoleAt($restaurant->id))

<h4>Horario "{{$scheduler->description}}"</h4>
<div id="organizeScheduler" @if($role == 'A')class="editable" @endif></div>

@push('styles')
<link rel="stylesheet" type="text/css" href="/manager/css/fullcalendar.min.css"/>
<link rel="stylesheet" type="text/css" href="/manager/css/scheduler.min.css"/>
@endpush

@push('scripts')
<script type="text/javascript" src="/manager/js/moment-with-locales.js"></script>
<script type="text/javascript" src="/manager/js/fullcalendar.min.js"></script>
<script type="text/javascript" src="/manager/js/scheduler.min.js"></script>
<script type="text/javascript" src="/manager/js/horario_scheduler.js"></script>
<script type="text/javascript">
	var sch_days = {!! json_encode($scheduler_days) !!};

	$('#organizeScheduler').fullCalendar('renderEvents', sch_days, true);

	@if($role == 'A')
	function create_day(week_day, start, end){
		loader_bar(true);

		var id = "{!! $restaurant->id.'_'.$scheduler->id.'_' !!}"+Math.floor(Math.random()*2000);

		var myEvent = {
			id:id,
			start: start,
			end: end,
			resourceId:week_day
		};

		$('#organizeScheduler').fullCalendar('renderEvent', myEvent, true);

		var data = {
			"sch_day_hour_start": start.format('HH:mm:ss'),
			"sch_day_hour_end": end.format('HH:mm:ss'),
			"sch_day_week_day": week_day,
			"sch_day_code": id,
		};

		$.ajax({
			type: "POST",
			url: "{!! route('restaurant.schedulers.ajax.sch_day.create', [$restaurant->id, $scheduler->id]) !!}",
			data: data,
		}).done(function(msg) {
			console.info('Created event!');
		}).fail(function(jqXHR, textStatus) {
			console.error(textStatus, jqXHR);
			$('#organizeScheduler').fullCalendar('removeEvents',id);
		}).then(function(){
			loader_bar(false);
		});
	};

	function update_day(code, week_day, start, end, revert){
		loader_bar(true);

		var data = {
			"sch_day_hour_start": start.format('HH:mm:ss'),
			"sch_day_hour_end": end.format('HH:mm:ss'),
			"sch_day_week_day": week_day,
			"sch_day_code": code,
		};

		$.ajax({
			type: "POST",
			url: "{!! route('restaurant.schedulers.ajax.sch_day.update', [$restaurant->id, $scheduler->id]) !!}",
			data: data,
		}).done(function(msg) {
			console.info('Updated event!');
		}).fail(function(jqXHR, textStatus) {
			console.error(textStatus, jqXHR);
			revert();
		}).then(function(){
			loader_bar(false);
		});
	}

	function remove_day(code){
		loader_bar(true);

		$.ajax({
			type: "POST",
			url: "{!! route('restaurant.schedulers.ajax.sch_day.remove', [$restaurant->id, $scheduler->id]) !!}",
			data: {
				"sch_day_code": code
			},
		}).done(function(msg) {
			console.info('Deleted event!');
			$('#organizeScheduler').fullCalendar('removeEvents',code);
		}).fail(function(jqXHR, textStatus) {
			console.error(textStatus, jqXHR);
		}).then(function(){
			loader_bar(false);
		});
	}
	@endif
</script>
@endpush

@endsection
