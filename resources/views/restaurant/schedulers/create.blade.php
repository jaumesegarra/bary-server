@extends('layouts.restaurant')

@section('main_content')
<h4>{{ (isset($scheduler)) ? 'Editar horario "'.$scheduler->description.'"' : 'Crear nuevo horario' }}</h4>

@php
$route = ['restaurant.schedulers.store', $restaurant->id];
$method = 'POST';

if(isset($scheduler)){
$method = 'PUT';
$route[0] = 'restaurant.schedulers.update';
$route[] = $scheduler->id;
}
@endphp

{{ Form::open(['class' => 'form max-medium', 'route' => $route, 'method' => $method]) }}
<p class="info">Recuerde que las fechas no pueden pertenecer a otro horario creado.</p>

<div class="columns col2 colm1">
	<div class="form-field">
		<label>Fecha inicio:
			<input type="text" class="{{ $errors->has('date_start') ? 'error' : '' }}" name="date_start" placeholder="dd/mm/yyyy" value="{{ (old('date_start')) ? old('date_start') : ((isset($scheduler)) ? (new Carbon\Carbon($scheduler->date_start))->format('d/m/Y') : '') }}" required/>
		</label>

		@if ($errors->has('date_start'))
		<span class="msg error">
			<strong>{{ $errors->first('date_start') }}</strong>
		</span>
		@endif
	</div>
	<div class="form-field">
		<label>Fecha fin:
			<input type="text" class="{{ $errors->has('date_end') ? 'error' : '' }}" name="date_end" placeholder="dd/mm/yyyy" value="{{ (old('date_end')) ? old('date_end') : ((isset($scheduler) && $scheduler->date_end) ? (new Carbon\Carbon($scheduler->date_end))->format('d/m/Y') : '') }}"/>
		</label>

		@if ($errors->has('date_end'))
		<span class="msg error">
			<strong>{{ $errors->first('date_end') }}</strong>
		</span>
		@endif
	</div>
	<div class="form-field">
		<label>Descripción:
			<input type="text" class="{{ $errors->has('description') ? 'error' : '' }}" name="description" value="{{ (old('description')) ? old('description') : ((isset($scheduler)) ? $scheduler->description : '') }}" required/>
		</label>

		@if ($errors->has('description'))
		<span class="msg error">
			<strong>{{ $errors->first('description') }}</strong>
		</span>
		@endif
	</div>
	<div class="form-field">
		<label>Aforo máximo de reservas:
			<input type="number" class="{{ $errors->has('capacity') ? 'error' : '' }}" name="capacity" value="{{ (old('capacity')) ? old('capacity') : ((isset($scheduler)) ? $scheduler->capacity : '25') }}" min="25" max="400" required/>
		</label>

		@if ($errors->has('capacity'))
		<span class="msg error">
			<strong>{{ $errors->first('capacity') }}</strong>
		</span>
		@endif
	</div>
	<div class="form-field">
		<label>Tiempo medio clientes (min.):
			<input type="number" class="{{ $errors->has('average_time') ? 'error' : '' }}" name="average_time" value="{{ (old('average_time')) ? old('average_time') : ((isset($scheduler)) ? $scheduler->average_time : '30') }}" min="30" max="180" step="30" required/>
		</label>

		@if ($errors->has('average_time'))
		<span class="msg error">
			<strong>{{ $errors->first('average_time') }}</strong>
		</span>
		@endif
	</div>
</div>
<div class="foot">
	<button type="submit" class="button success">
		Guardar
	</button>
</div>
{{ Form::close() }}

@push('styles')
<link rel="stylesheet" type="text/css" href="/manager/css/melon.datepicker.css"/>
@endpush

@push('scripts')
<script type="text/javascript" src="/manager/js/moment-with-locales.js"></script>
<script type="text/javascript">
	$("input[name='date_start'], input[name='date_end']").datepicker({
		beforeShow: function() {
			$(this).datepicker("widget").wrap('<div class="ll-skin-melon"></div>');
		},
		onClose: function() {
			$(this).datepicker("widget").hide();
			$(this).datepicker("widget").unwrap();
		}
	});

	update_min_end();

	function update_min_end(){
		var min_date = ($("input[name='date_start']").val().trim() != "") ? moment($("input[name='date_start']").val(), "DD/MM/YYYY", true).add(1, 'd').toDate() : new Date();

		$("input[name='date_end']").datepicker("option", "minDate", min_date);
	}

	$("input[name='date_start']").datepicker("option", "onSelect", function(dateText) {
		update_min_end();
	});
</script>
@endpush
@endsection
