@extends('layouts.restaurant')

@section('main_content')
@php ($role = Auth::guard()->user()->hasRoleAt($restaurant->id))

<h4><!--Reservas:--></h4>
<div id="reservesScheduler" class="@if(in_array($role, ['A', 'W']))editable @endif"></div>
@push('styles')
<link rel="stylesheet" type="text/css" href="/manager/css/fullcalendar.min.css"/>
<link rel="stylesheet" type="text/css" href="/manager/css/melon.datepicker.css"/>
<link rel="stylesheet" type="text/css" href="/manager/css/jquery.qtip.min.css"/>
<link rel="stylesheet" type="text/css" href="/manager/css/event-manager.css"/>
@endpush
@push('scripts')
<script type="text/javascript" src="/manager/js/moment-with-locales.js"></script>
<script type="text/javascript" src="/manager/js/fullcalendar.min.js"></script>
<script type="text/javascript" src="/manager/js/jquery.qtip.min.js"></script>

<script type="text/javascript">var GET_EVENTS_URL = "{!!route('restaurant.reserves.ajax.get', $restaurant->id)!!}";@if(in_array($role, ['A', 'W']))var GET_AVHOURS_OF_DAY = "{!! route('restaurant.reserves.ajax.av_hours', [$restaurant->id]) !!}";var CREATE_URL = "{!! route('restaurant.reserves.ajax.create', [$restaurant->id]) !!}";var FINALIZE_URL = "{!! route('restaurant.reserves.ajax.finalize', [$restaurant->id, '_i_']) !!}";var CANCEL_URL = "{!! route('restaurant.reserves.ajax.cancel', [$restaurant->id, '_i_']) !!}";@endif</script>
<script type="text/javascript" src="/manager/js/event-manager.js"></script>
@endpush
@endsection