@extends('layouts.restaurant')

@section('main_content')
@parent
<div class="charts">
	<div class="reserve_days">
		<h4>Reservas esta semana</h4>
		<div>
			<canvas data-value="{!!$week_reserves!!}"></canvas>
		</div>
	</div>
	<div class="reserve_hours">
		<h4>Reservas hoy</h4>
		<canvas data-value='{!!$today_reserves!!}'></canvas>
	</div>
</div>
<hr style="margin: 25px 0px 0px;border-color: #eee;">
<h4 style="margin: 25px 10px 5px;">Reservas por dia</h4>
<div class="full_days" style="display: none;">
	<div class="ll-skin-melon"><div class="calendar"></div></div>
	<div class="reserves_data">
		<p>Loading...</p>
	</div>
</div>
<p class="no-found full_days_none_js" style="display: none">
	No hay días de apertura próximos.
</p>
@push('styles')
<link rel="stylesheet" type="text/css" href="/manager/css/melon.datepicker.css"/>
<link rel="stylesheet" type="text/css" href="/manager/css/home-manage.css"/>
@endpush
@push('scripts')
<script type="text/javascript" src="/manager/js/moment-with-locales.js"></script>
<script type="text/javascript" src="/manager/js/chart.bundle.min.js"></script>
<script type="text/javascript">
	var GET_AVAILABLE_DATES = "{!! route('restaurant.index.ajax.available_dates', [$restaurant->id]) !!}";
	var GET_RESERVES_DATA = "{!! route('restaurant.index.ajax.reserves_data', [$restaurant->id, '']) !!}";
</script>
<script type="text/javascript" src="/manager/js/home-manage.js"></script>
@endpush
@endsection