@extends('layouts.restaurant')

@section('main_content')
@parent
<div class="charts">
	<div class="reserve_months">
		<h4>Reservas por mes</h4>
		<canvas></canvas>
	</div>
	
	<div class="scheduler-stats">
		<div class="week_average_time">
			<h4>Tiempo medio de estancia en min. (horario actual) [{{$total_average_time}} min.]</h4>
			<canvas data-value="{!! $week_average_time !!}"></canvas>
		</div>

		<div class="reserve_types">
			<h4>Cantidad de reservas (horario actual)</h4>
			<div>
				<canvas data-value="{!! $reserve_types !!}"></canvas>
			</div>
		</div>
	</div>
</div>
@push('styles')
<link rel="stylesheet" type="text/css" href="/manager/css/statistics.css"/>
@endpush
@push('scripts')
<script type="text/javascript" src="/manager/js/moment-with-locales.js"></script>
<script type="text/javascript" src="/manager/js/chart.bundle.min.js"></script>
<script type="text/javascript">
	var MONTH_RESERVES = {!! $month_reserves !!};
</script>
<script type="text/javascript" src="/manager/js/statistics.js"></script>
@endpush
@endsection