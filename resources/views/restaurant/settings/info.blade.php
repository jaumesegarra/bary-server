@extends('restaurant.settings.index')

@section('settings_content')
@parent

{{ Form::open(['class' => 'form max-medium', 'route' => ['restaurant.settings.info.save', $restaurant->id], 'method' => 'PUT']) }}
<div class="columns col2 colm1">
	<div class="form-field">
		<label>Nombre
			<input type="text" class="{{ $errors->has('name') ? 'error' : '' }}" name="name" value="{{ (old('name')) ? old('name') : $restaurant->i()->name }}" required autofocus @if($restaurant->approbed == 2) disabled @endif/>
		</label>

		@if ($errors->has('name'))
		<span class="msg error">
			<strong>{{ $errors->first('name') }}</strong>
		</span>
		@endif
	</div>
	<div class="form-field">
		<label>Ciudad
			<select name="city_id" value="{{ (old('city_id')) ? old('city_id') : $restaurant->i()->city_id }}" @if($restaurant->approbed == 2) disabled @endif>
				<option value="">Select one...</option>

				@foreach($cities as $city)
				<option value="{{$city->id}}">{{$city->name}}, {{$city->country}}</option>
				@endforeach
			</select>
		</label>

		@if ($errors->has('city_id'))
		<span class="msg error">
			<strong>{{ $errors->first('city_id') }}</strong>
		</span>
		@endif
	</div>

	<div class="form-field">
		<label>Dirección
			<textarea name="address" required="required" @if($restaurant->approbed == 2) disabled @endif>{{ (old('address')) ? old('address') : $restaurant->i()->address }}</textarea>
		</label>

		@if ($errors->has('address'))
		<span class="msg error">
			<strong>{{ $errors->first('address') }}</strong>
		</span>
		@endif
	</div>
	<div class="form-field">
		<label>Descripción
			<textarea name="description" required="required" @if($restaurant->approbed == 2) disabled @endif>{{ (old('description')) ? old('description') : $restaurant->i()->description }}</textarea>
		</label>

		@if ($errors->has('description'))
		<span class="msg error">
			<strong>{{ $errors->first('description') }}</strong>
		</span>
		@endif
	</div>
	<div class="form-field">
		<label>Etiquetas (máximo 10) <span class="info">[Separadas por comas]</span>
			<textarea name="tags" class="{{ $errors->has('tags') ? 'error' : '' }}" @if($restaurant->approbed == 2) disabled @endif>{{ (old('tags')) ? old('tags') : $restaurant_tags }}</textarea>
		</label>

		@if ($errors->has('tags'))
		<span class="msg error">
			<strong>{{ $errors->first('tags') }}</strong>
		</span>
		@endif
	</div>
	<div class="form-field">
		<label>Teléfono
			<input type="text" name="phone" required="required" value="{{ (old('phone')) ? old('phone') : $restaurant->i()->phone }}" @if($restaurant->approbed == 2) disabled @endif/>
		</label>

		@if ($errors->has('phone'))
		<span class="msg error">
			<strong>{{ $errors->first('phone') }}</strong>
		</span>
		@endif
	</div>
</div>
@if($restaurant->approbed != 2)
<div class="foot">
	<button type="submit" class="button small success">
		Guardar
	</button>
</div>
@endif
{{ Form::close() }}

@push('styles')
<style type="text/css">
	form{
		margin-top: 20px
	}
</style>
@endpush
@push('scripts')
<script type="text/javascript">
	$("select[name='city_id']").combobox();

	function split( val ) {
		return val.split( /,\s*/ );
	}

	function extractLast( term ) {
		return split( term ).pop();
	}

	$("textarea[name='tags']")
	.on( "keydown", function(event) {
		if(event.keyCode === $.ui.keyCode.TAB && $(this).autocomplete( "instance" ).menu.active ) {
			event.preventDefault();
		}
	})
	.autocomplete({
		source: function(request, response) {
			$.getJSON("{{route('assistant.ajax.restaurant_tags')}}", {
				term: extractLast( request.term )
			}, response );
		},
		search: function() {
			var term = extractLast( this.value );
			if (term.length < 1) {
				return false;
			}
		},
		focus: function() {
			return false;
		},
		select: function(event, ui){
			var terms = split( this.value );
			terms.pop();
			terms.push( ui.item.value );
			terms.push( "" );
			this.value = terms.join( ", " );
			return false;
		}
	});
</script>
@endpush
@endsection