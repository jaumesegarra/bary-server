@extends('restaurant.settings.index')

@section('settings_content')
@parent
<label class="search">
	Buscar usuario:
	<input type="text" name="manager"/>
</label>

{{ Form::open(['class' => 'form max-medium', 'route' => ['restaurant.settings.managers.save', $restaurant->id], 'method' => 'POST']) }}
<div class="managers">
	@foreach($restaurant->managers as $manager)
	<div data-userid="{{$manager->id}}">
		<input type="hidden" name="user[]" value='{"id": {{$manager->id}}, "role": "{{$manager->pivot->role}}"}' />
		<div class="info ellipsis">
			<font class="name ellipsis">{{$manager->name}}</font>
			<span class="email ellipsis">{{$manager->email}}</span>
		</div>
		@if(Auth::id() == $manager->id)
		<div class="me" title="Eres tu!">
			<i class="la la-user"></i>
		</div>
		@endif
		@if($manager->pivot->activation)
		<div class="confirmation" title="El usuario deberá aprobarlo vía email">
			<i class="la la-question"></i>
		</div>
		@endif
		<div class="role">
			<div class="select-wrapper">
				<select name="userRole[]" @if(Auth::id() == $manager->id ||$manager->pivot->activation) disabled="disabled" @endif>
					<option value="A" @if($manager->pivot->role == 'A') selected="selected"@endif>Administrador</option>
					<option value="W" @if($manager->pivot->role == 'W') selected="selected"@endif>Camarero</option>
					<option value="S" @if($manager->pivot->role == 'S') selected="selected"@endif>Analista</option>
				</select>
			</div>
		</div>
		<div class="delete">
			@if($manager->pivot->role != 'A' || $num_admins > 1)
			<a href="#" class="delete_manager_js">
				<i class="la la-trash"></i>
			</a>
			@endif
		</div>
	</div>
	@endforeach
</div>
<div class="foot">
	<button class="button small">Guardar</button>
</div>
{{ Form::close() }}
@push('styles')
<style type="text/css">
label.search{
	display: block;
	padding: 10px 0;
}
label.search input{
	display: table;
	margin-top: 5px;
	width: calc(100% - 30px);
	max-width: 320px
}
.managers{
	margin-top: 15px
}
.managers > div{
	display: flex;
	align-items: center;
	position: relative;
	border-bottom: 1px solid #ddd;
	margin-bottom: 5px;
	padding: 10px 0;
}
.managers div.info{
	margin-right: auto;
}
.managers div.info .name{
	display: block;
}
.managers div.info .email{
	margin-top: 5px;
	font-size: 14px;
	color:#777;
	display: block;
}
.managers div.confirmation, .managers div.me{
	margin-left: 10px
}
.managers div.role{
	margin-left: 10px;
	width: 145px
}
.managers div.delete{
	display: block;
	width: 18px;
	margin:10px;
}
</style>
@endpush
@push('scripts')
<script type="text/javascript">
	var managers = JSON.parse('{"users": {!! json_encode($restaurant->managers()->pluck("id"))!!} }');

	$("input[name='manager']").autocomplete({
		minLength: 0,
		source: function(request, response) {
			$.getJSON("{{route('restaurant.settings.managers.ajax.get_users', [$restaurant->id])}}", {
				term: request.term
			}, response );
		},
		select: function( event, ui ) {
			$("input[name='manager']").val('');

			if($('.managers div[data-userid="'+ui.item.id+'"]').length == 0)
				create_element(ui.item);

			return false;
		}
	})
	.autocomplete( "instance" )._renderItem = function(ul, item) {
		return $("<li>")
		.append("<div>"+item.name+"<br> <span style='font-size:12px'>"+item.email+"</span></div>")
		.appendTo(ul);
	};

	function create_element(item) {
		var is_new = (managers.users.indexOf(item.id) == -1);

		var $div = $(
		             '<div data-userid="'+item.id+'">'+
		             '<input type="hidden" name="user[]" value=\'{"id": '+item.id+', "role": "W"}\' />'+
		             '<div class="info ellipsis">'+
		             '<font class="name ellipsis">'+item.name+'</font>'+
		             '<span class="email ellipsis">'+item.email+'</span>'+
		             '</div>'+
		             ((is_new) ? '<div class="confirmation" title="El usuario deberá aprobarlo vía email"><i class="la la-question"></i></div>' : '')+
		             '<div class="role">'+
		             '<div class="select-wrapper">'+
		             '<select name="userRole[]" value="W"><option value="A">Administrador</option><option value="W" selected="selected">Camarero</option><option value="S">Analista</option></select>'+
		             '</div>'+
		             '</div>'+
		             '<div class="delete">'+
		             '<a href="#" class="delete_manager_js">'+
		             '<i class="la la-trash"></i>'+
		             '</a>'+
		             '</div>'+
		             '</div>'
		             );
		$('.managers').append($div);
	}

	$('.managers').on('change', 'select', function(){
		var value = $(this).val();
		var $dataInput = $('input[name="user[]"]', $(this).parents('div[data-userid]'));

		var data = JSON.parse($dataInput.val());
		data.role = value;
		$dataInput.val(JSON.stringify(data));
	});

	$('.managers').on('click', '.delete_manager_js', function (e) {
		e.preventDefault();
		$(this).parents('div[data-userid]').remove();
	});
</script>
@endpush
@endsection