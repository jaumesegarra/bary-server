@extends('layouts.manage')

@section('content')
    <h2><font class="ellipsis">Dashboard</font> <a href="{{route('assistant.create')}}" class="button small">Crear nuevo</a></h2>

    <section class="background-white" id="restaurants">
        <h6>Mis restaurantes</h6>
        <ul class="restaurant-list">
            @forelse($restaurants as $restaurant)
            <a href="{{route('restaurant.index', $restaurant->id)}}">
                <li>
                    <div class="name ellipsis">{{$restaurant->name}} <b>({{$restaurant->city->name}})</b></div>
                    <div class="tags">
                        <span class="role">{{\App\Manager::$ROLE[$restaurant->pivot->role]}}</span>
                        @if($restaurant->approbed == 0)
                        <span class="pending_approbal">No aprobado</span>
                        @endif
                        @if($restaurant->approbed == 2)
                        <span class="pending_approbal">En espera</span>
                        @endif
                    </div>
                </li>
            </a>
            @empty 
            <p class="no-found" style="margin: 20px 5px 10px;">
                No ha registrado todavía ningún restaurante ni se le ha dado permisos en alguno...
            </p>
            @endforelse
        </ul>
    </section>
@endsection
