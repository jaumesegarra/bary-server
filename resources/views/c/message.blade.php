@extends('layouts.mini')

@section('content')
<div class="msg {{ ($type) ? $type : 'success' }}">
	{!! $message !!}
</div>
@if ($redirect)
<meta http-equiv="refresh" content="5; url=/" />
<p class="info">
	En 5 segundos será redirigido a la pagina de inicio...
</p>
@endif

@if (isset($script))
@push('scripts')
<script type="text/javascript">
	{!!$script!!}
</script>
@endpush
@endif
@endsection
