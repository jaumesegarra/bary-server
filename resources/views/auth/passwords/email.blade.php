@extends('layouts.app')

@section('content')
<div id="presentation">
    <div class="title">
        <div class="center">
            <div class="card">
                <div class="card-content">
                    <h2>Recuperar contraseña</h2>

                    @if (session('status'))
                    <div class="msg success">
                        {{ session('status') }}
                    </div>
                    @endif

                    @if ($errors->has('email'))
                    <div class="msg error">
                        <strong>{{ $errors->first('email') }}</strong>
                    </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <div class="form-field">
                            <label>
                                <input id="email" type="email" placeholder="Email" class="{{ $errors->has('email') ? ' error' : '' }}" name="email" value="{{ old('email') }}" required>
                            </label>
                        </div>

                        <div class="foot">
                            <a href="{{ route('login') }}">Volver</a>
                            <button type="submit" class="button">
                                Enviar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="background"></div>
</div>
@push('styles')
<link rel="stylesheet" type="text/css" href="/manager/css/presentation.css"/>
<link rel="stylesheet" type="text/css" href="/manager/css/auth-card.css"/>
@endpush
@endsection
