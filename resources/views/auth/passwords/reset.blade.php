@extends('layouts.app')

@section('content')
<div id="presentation">
    <div class="title">
        <div class="center">
            <div class="card">
                <div class="card-content">
                    <h2>Resetear contraseña</h2>

                    <form method="POST" action="{{ route('password.request') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-field">
                            <label>
                                <input id="email" type="email" placeholder="Email" class="{{ $errors->has('email') ? ' error' : '' }}" name="email" value="{{ $email or old('email') }}" required autofocus>
                            </label>

                            @if ($errors->has('email'))
                            <div class="msg error">
                                <strong>{{ $errors->first('email') }}</strong>
                            </div>
                            @endif
                        </div>

                        <div class="form-field">
                            <label>
                                <input id="password" type="password" placeholder="Nueva contraseña" class="{{ $errors->has('password') ? ' has-error' : '' }}" name="password" required>
                            </label>

                            @if ($errors->has('password'))
                            <div class="msg error">
                                <strong>{{ $errors->first('password') }}</strong>
                            </div>
                            @endif
                        </div>

                        <div class="form-field">
                            <label>
                                <input id="password-confirm" type="password" placeholder="Confirmar contraseña" class="{{ $errors->has('password_confirmation') ? ' has-error' : '' }}" name="password_confirmation" required>
                            </label>

                            @if ($errors->has('password_confirmation'))
                            <div class="msg error">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </div>
                            @endif
                        </div>

                        <div class="foot">
                            <button type="submit" class="button">
                                Cambiar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="background"></div>
</div>
@push('styles')
<link rel="stylesheet" type="text/css" href="/manager/css/presentation.css"/>
<link rel="stylesheet" type="text/css" href="/manager/css/auth-card.css"/>
@endpush
@endsection
