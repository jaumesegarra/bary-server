@extends('admin.structure')

@section('main_content')
@parent

<div class="card-list"> 
	@forelse ($restaurants as $restaurant)
	@php($photo = $restaurant->photos->first())
	<a href="restaurant" data-id="{{$restaurant->id}}" href-modal load-json="{{route('admin.ajax.rta', $restaurant->id)}}">
	<div class="card">
		<div class="card-thumbnail{{($photo) ? ' image-exists' : ''}}" {!! ($photo) ? 'style="background-image: url('.$photo->getThumbUrl().')"' : '' !!}></div>
		<div class="card-content">
			<h3 class="title">{{ $restaurant->name }}</h3>
			<font class="small ellipsis">{{$restaurant->city->name}}, {{$restaurant->city->country}}</font>
			<div class="icons">
				@if($restaurant->schedulers_days_count > 0) 
					<i class="la la-calendar-o"></i>
				@endif
				@if($restaurant->menu_items_count > 0) 
					<i class="la la-cutlery"></i>
				@endif
				@if($restaurant->photos_count > 0) 
					<i class="la la-camera"></i>
				@endif
			</div>
		</div>
		{{ Form::open(['class' => 'approve_restaurant_form_js', 'route' => ['admin.approve_restaurant', $restaurant->id], 'method' => 'POST']) }}
		{{ Form::close() }}

		{{ Form::open(['class' => 'deny_restaurant_form_js', 'route' => ['admin.deny_restaurant', $restaurant->id], 'method' => 'POST']) }}
			<input type="hidden" value="" name="reason"/>
		{{ Form::close() }}
	</div>
	</a>
	@empty
	<p class="no-found">No hay restaurantes pendientes de aprobación en estos momentos.</p>
	@endforelse

	{{ $restaurants->links() }}
</div>


@push('styles')
<link rel="stylesheet" href="/manager/css/vex.css" />
<link rel="stylesheet" href="/manager/css/vex-theme-top.css" />
<style type="text/css">
	.modal{
		z-index:111;
	}
	.modal .modal-body{
		width:calc(100% - 60px);
		max-width: 900px;
		overflow:auto;
		max-height: calc(100vh - 70px);
	}
	.details {
	    padding: 0;
	    list-style-type: none;
	    margin: 0;
	}
	.details li {
	    display: flex;
	    align-items: center;
	    margin: 5px 0;
	}
	.details li > i {
	    font-size: 21px;
	}
	.details li > div {
	    margin: 0 10px;
	}
	.details li > div h6 {
	    margin: 5px 0;
	}
	.details li > div font {
	    color: #989696;
	    font-weight: 100;
	    font-size: 16px;
	}
	.description{
		margin-top: 20px
	}
	.description p{
		max-height:100px;
		overflow:auto;
		word-break:break-all
	}
	.images{
		margin-top:20px
	}
	.images .card-overflow{
		margin-top: 15px
	}
	.images .card-overflow .card{
		width:205px
	}
	.images .card-overflow .card .card-content {
	    padding: 5px 15px;
	}
	.menu{
		margin-top: 25px
	}
	.menu h2{
	    font-weight: 100 !important;
	    color: #a2a2a2;
	    font-size: 18px;
	}
	.menu hr{
		margin: 0px 0px 15px;
	}
</style>
@endpush
@push('scripts')
<script src="/manager/js/vex.combined.min.js"></script>
<script>vex.defaultOptions.className = 'vex-theme-top'</script>
<script type="text/javascript">
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	
	function loader_bar(show){
		var $bar = $('span.loading-bar');
		if (show)
			$bar.addClass('show');
		else
			$bar.removeClass('show');
	}

	var restaurantModal = function(data){
		console.log(data);
		var template = ` 
				<h2>
					`+data.name+`
					<div class="right">
						<button class="button small grey js_deny" data-id="`+data.id+`">DENEGAR</button>
						<button class="button small js_approbe" data-id="`+data.id+`">APROBAR</button>
					</div>
				</h2>
				<ul class="details">
					<li>
						<i class="la la-minus-square"></i>
						<div>
							<h6>Tipo</h6>
							<font>`+data.type+`</font>
						</div>
					</li>
					<li>
						<i class="la la-map-marker"></i>
						<div>
							<h6>Dirección</h6>
							<font>`+data.address+`</font> <b style="font-size: 14px">`+data.city+`</b>
						</div>
					</li>
					<li>
						<i class="la la-phone"></i>
						<div>
							<h6>Teléfono</h6>
							<font>`+data.phone+`</font>
						</div>
					</li>
					<li>
						<i class="la la-calendar-o"></i>
						<div>
							<h6>Horario</h6>
							<font `+(!data.hasScheduler ? 'style="color:red"' : '')+`>`+((data.hasScheduler) ? 'Si' : 'No')+`</font>
						</div>
					</li>
				</ul>
				
				<div class="description">
					<h5>Descripción</h5>
					<p>`+data.description+`</p>
				</div>
				
				<div class="tags">`;
					for(var i=0; i < data.tags.length; i++){
						template += `<span class="tag">`+data.tags[i]+`</span>`;
					}		
   template += `</div>

				<div class="images">
					<h5>Imágenes</h5>`;
		if(data.photos.length > 0){
	   template += `<div class="card-overflow">`;
						for(var i=0; i < data.photos.length; i++){
							var photo = data.photos[i];

							template += `<div class="card">
											<div class="card-thumbnail image-exists" style="background-image:url(`+photo.thumb+`)"></div>
											<div class="card-content">
												<p>
													`+photo.description+`
												</p>
											</div>
										</div>`;
						};
		template +=	`</div>`;
		}else template += `<p class="no-found">No hay imagenes.</p>`;
		template += `<div class="menu">
						<h2>Menú</h2>
						<hr/>
		`;
		if(data.menu.length > 0){
			for(var i=0; i < data.menu.length; i++){
				var section = data.menu[i];
			template += `<div class="menu-section">
							<div class="menu-section-head">
								<h3>`+section.title+`</h3>
							</div>	
							<div class="menu-items">`;
								for(var j=0; j < section.items.length; j++){
									var item = section.items[j];

									template += `<div class="menu-item">
													<div class="menu-item-head">
														<h5>`+item.name+`</h5>
														<p>`+item.description+`</p>
													</div>
													<div class="menu-item-right">
														<font class="price">`+item.price+`</font>
													</div>
												</div>`;
								}
				template +=	`</div>
						</div>`;
			}
		}else template += `<p class="no-found">No disponible.</p>`;

	template +=	`	</div>
				</div>
		`;

		return template;
	}

	$('body').on('click', '[href-modal]', function(e){
		e.preventDefault();
		
		var data = null;
		if($(this).attr('let-data'))
			data = JSON.parse($(this).attr('let-data'));

		var $modal = $('<div class="modal"></div>');
			$modal.appendTo('body');

		var ref_modal = $(this).attr('href');
				
		function open_modal(d){
			$modal_content = $('<div class="modal-body"></div>');
			$modal_content.html(window[ref_modal+'Modal'](d));

			$modal.append($modal_content);
		}

		function close_modal(){
			$modal.remove();
		}

		$modal.on('click', function(e){
			if($(e.target).hasClass('modal'))
				close_modal();
		});
	
		if($(this).attr('load-json')){
			loader_bar(true);

			$.ajax({
				type: "GET",
				url: $(this).attr('load-json')
			}).done(function(data) {
								
				open_modal(data);

			}).fail(function(jqXHR, textStatus) {
				console.error(textStatus, jqXHR);
				close_modal();
			}).then(function(){
				loader_bar(false);
			});
		}else open_modal(data);
	});

	$('body').on('click', '.js_approbe', function (e) {
		e.preventDefault();

		var id = $(this).data('id');

		vex.dialog.confirm({
			message: "¿Está seguro/a que desea aprobar este restaurante?",
			callback: function (value) {
				if (value){
					var $form = $('.card-list a[data-id="'+id+'"] .approve_restaurant_form_js');
					$form.submit();
				}
			}
		});
	});

	$('body').on('click', '.js_deny', function (e) {
		e.preventDefault();

		var id = $(this).data('id');

		vex.dialog.prompt({
		    message: '¿Está seguro/a que desea denegar la solicitud de aprobación de este restaurante?',
		    placeholder: 'Motivo',
		    callback: function (value) {
		        if(value && value.trim() != ''){
		        	var $form = $('.card-list a[data-id="'+id+'"] .deny_restaurant_form_js');
					$('input[name="reason"]', $form).val(value);
					$form.submit();
		        }
		    }
		})
	});
</script>
@endpush
@endsection