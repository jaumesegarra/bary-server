@extends('layouts.admin')

@section('content')
@parent
<span class="loading-bar"></span>
<main>
	<section class="background-white">
		<h2>
			<font class="logo">
				<img src="/assets/img/sm_icon.png"/> 
				<span>Bary admin</span>
			</font>
			
			<div class="right">
				{{ Form::open(['route' => 'admin.logout', 'method' => 'POST']) }}
				<button type="submit" class="button small grey">
					<i class="la la-sign-out"></i>
					Cerrar sesión
				</button>
				{{ Form::close() }}
			</div>
		</h2>
		
		<div class="subnav">
			<a href="{{route('admin.index')}}" @if(Route::current()->getName() == 'admin.index') class="active" @endif>Aprobar</a>
			<a href="{{route('admin.changes')}}" @if(Route::current()->getName() == 'admin.changes') class="active" @endif>Revisar cambios</a>
		</div>

		@yield('main_content')
	</section>
</main>
@push('styles')
<link rel="stylesheet" type="text/css" href="/manager/css/admin.css"/>
@endpush
@endsection