@extends('layouts.admin')

@section('content')
@parent

<div class="card">
	<div class="card-content">
		
        <h2><img src="/assets/img/sm_icon.png"/> Bary admin</h2>
        @if ($errors->has('email') || $errors->has('password'))
        <div class="msg error">
            <strong>Credenciales incorrectas</strong>
        </div>
        @endif

        <form method="POST" action="{{ route('admin.login') }}">
            {{ csrf_field() }}

            <div class="form-field">
                <label>
                    <input id="email" type="email" placeholder="Email" name="email" class="{{ $errors->has('email') || $errors->has('password') ? ' error' : '' }}" value="{{ old('email') }}" required autofocus>
                </label>
            </div>

            <div class="form-field">
                <label>
                    <input id="password" type="password" placeholder="Contraseña" class="{{ $errors->has('email') || $errors->has('password') ? ' error' : '' }}" name="password" required>
                </label>
            </div>

            <div class="foot">
                <button type="submit" class="button">
                    Login
                </button>
            </div>
        </form>
    </div>
</div>
@push('styles')
<link rel="stylesheet" type="text/css" href="/manager/css/auth-card.css"/>
<link rel="stylesheet" type="text/css" href="/manager/css/admin.css"/>
<style type="text/css">
    .card{
        margin-top: 10vh;
    }
</style>
@endpush
@endsection