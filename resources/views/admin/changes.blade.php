@extends('admin.structure')

@section('main_content')
@parent
<div class="card-list">
	@forelse ($restaurants as $indexKey => $restaurant)
		@php($changes = $restaurant->changes)
		<div class="card">
			<div class="card-content">
				<h5>
					{{$restaurant->name}}

					<a class="expand">
						<i class="la {{ ($indexKey == 0) ? 'la-chevron-up' : 'la-chevron-down'}}"></i>
					</a>
				</h5>
				
				<div class="card content-collapsed" @if($indexKey == 0) style="display: block;"@endif>
					<div class="card-content">
						<h6>Cambios:</h6>
						<table border="0">
							<tr>
								<th>Campo</th>
								<th>Actual</th>
								<th>Nuevo</th>
							</tr>
							@if($changes->name)
							<tr>
								<td>
									Nombre
								</td>
								<td>
									{{$restaurant->name}}
								</td>
								<td>
									{{$changes->name}}
								</td>
							</tr>
							@endif
							@if($changes->phone)
							<tr>
								<td>Teléfono</td>
								<td>
									{{$restaurant->phone}}
								</td>
								<td>
									{{$changes->phone}}
								</td>
							</tr>
							@endif
							@if($changes->address)
							<tr>
								<td>Dirección</td>
								<td>
									{{$restaurant->address}}
								</td>
								<td>
									{{$changes->address}}
								</td>
							</tr>
							@endif
							@if($changes->city_id)
							<tr>
								<td>Ciudad</td>
								<td>
									{{$restaurant->city->name}}, {{$restaurant->city->country}}
								</td>
								<td>
									{{$changes->city->name}}, {{$changes->city->country}}
								</td>
							</tr>
							@endif
							@if($changes->description)
							<tr>
								<td>Descripción</td>
								<td>
									{{$restaurant->description}}
								</td>
								<td>
									{{$changes->description}}
								</td>
							</tr>
							@endif
						</table>
					</div>
					<div class="foot">
						{{ Form::open(['route' => ['admin.approve_changes', $restaurant->id], 'method' => 'POST']) }}
						{{ Form::close() }}

						<button class="button small aprove_js">Aprobar cambios</button>
					</div>
				</div>
			</div>
		</div>
	@empty
	<p class="no-found">No hay restaurantes con cambios pendientes en estos momentos.</p>
	@endforelse
</div>

{{ $restaurants->links() }}

@push('styles')
<link rel="stylesheet" href="/manager/css/vex.css" />
<link rel="stylesheet" href="/manager/css/vex-theme-top.css" />
<style type="text/css">
.card-content h5{
	z-index: 1;
	cursor: pointer;
}
.expand{
	position: absolute;
	right: 15px;
	top: 15px;
	z-index: 0
}
.card-list .card .card-content {
    width: 100%;
}
.card-list .card .card-content > .content-collapsed{
	display: none;
	margin-top: 15px
}
.content-collapsed h6{
	margin:5px 0;
}
table{
	width: calc(100% - 30px);
}
table th, table td {
	max-width: initial;
	width: 200px
}
.foot{
	padding: 5px 15px;
	display: flex;
}
.foot > *:last-child{
	margin-left: auto;
}
</style>
@endpush
@push('scripts')
<script src="/manager/js/vex.combined.min.js"></script>
<script>vex.defaultOptions.className = 'vex-theme-top'</script>
<script type="text/javascript">
	$('.card-content h5').on('click', function (e) {
		e.preventDefault();

		$('.content-collapsed', $(this).parents('.card-content')).toggle();

		$('.expand i', $(this).parents('.card-content')).attr('class', 'la '+($('.expand i', $(this).parents('.card-content')).hasClass('la-chevron-down') ? 'la-chevron-up' : 'la-chevron-down'));
	});

	$('.aprove_js').on('click', function (e) {
		e.preventDefault();

		var $form = $('form', $(this).parents('.foot'));

		vex.dialog.confirm({
			message: "¿Está seguro/a que desea aprobar estos cambios?",
			callback: function (value) {
				if (value)
					$form.submit();
			}
		});
	});
</script>
@endpush
@endsection