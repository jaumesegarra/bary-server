<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RestaurantMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_menu_section', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->string('icon');
            $table->string('title', 45);
            $table->integer('restaurant_id')->unsigned();
            $table->integer('order');

            $table->unique(['restaurant_id', 'order'], 'menu_section_order_unique');

            $table->foreign('restaurant_id')
            ->references('id')
            ->on('restaurant')
            ->onUpdate('cascade')->onDelete('cascade');
        });


        Schema::create('restaurant_menu_item', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->string('name', 45);
            $table->string('description', 250);
            $table->double('price',5,2);
            $table->integer('restaurant_menu_section_id')->unsigned();
            $table->integer('order');

            $table->unique(['restaurant_menu_section_id', 'order'], 'menu_item_order_unique');

            $table->foreign('restaurant_menu_section_id')
            ->references('id')
            ->on('restaurant_menu_section')
            ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_menu_section');
        Schema::dropIfExists('restaurant_menu_item');
    }
}
