<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RestaurantReview extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_review', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->string('title', 45)->nullable();
            $table->string('description', 250)->nullable();
            $table->integer('rate');
            $table->integer('restaurant_id')->unsigned();

            $table->timestamps();
            
            $table->primary(['restaurant_id', 'user_id'], 'restaurant_review_primary');

            $table->foreign('user_id')
            ->references('id')
            ->on('user')
            ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('restaurant_id')
            ->references('id')
            ->on('restaurant')
            ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_review');
    }
}
