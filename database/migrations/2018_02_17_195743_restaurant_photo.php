<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RestaurantPhoto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_photo', function (Blueprint $table) {
            $table->string('path')->primary();
            $table->string('format');
            $table->string('description');
            $table->integer('restaurant_id')->unsigned();
            $table->boolean('main');
            
            $table->timestamps();
            
            $table->foreign('restaurant_id')
            ->references('id')
            ->on('restaurant')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_photo');
    }
}
