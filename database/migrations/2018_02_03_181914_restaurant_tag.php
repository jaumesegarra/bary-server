<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RestaurantTag extends Migration
{
   /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
   {
    Schema::create('restaurant_tag', function (Blueprint $table) {
        $table->increments('id')->index();
        $table->string('name')->unique();
    });

    Schema::create('restaurant_restaurant_tag', function (Blueprint $table) {
        $table->integer('restaurant_id')->unsigned();
        $table->integer('restaurant_tag_id')->unsigned();

        $table->primary(['restaurant_id', 'restaurant_tag_id'], 'restaurant_restaurant_tag_primary');

        $table->foreign('restaurant_id')
        ->references('id')
        ->on('restaurant')
        ->onUpdate('cascade')->onDelete('cascade');

        $table->foreign('restaurant_tag_id')
        ->references('id')
        ->on('restaurant_tag')
        ->onUpdate('cascade')->onDelete('cascade');
    });
}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_tag');
        Schema::dropIfExists('restaurant_restaurant_tag');
    }
}
