<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RestaurantClosedDay extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_closed_day', function (Blueprint $table) {
            $table->integer('restaurant_id')->unsigned();
            $table->date('date');

            $table->primary(['restaurant_id', 'date'], 'restaurant_clday_primary');

            $table->foreign('restaurant_id')
            ->references('id')
            ->on('restaurant')
            ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_closed_day');
    }
}
