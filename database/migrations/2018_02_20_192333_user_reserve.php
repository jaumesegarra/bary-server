<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class UserReserve extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_reserve', function (Blueprint $table) {
            $table->string('code')->primary();
            $table->integer('user_id')->nullable()->unsigned();
            $table->integer('restaurant_id')->unsigned();

            $table->datetime('date');
            $table->datetime('date_end')->nullable();
            $table->integer('num_pers')->default(1);
            $table->string('suggestions')->nullable();
            $table->boolean('canceled')->default(false);
            $table->string('name')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
            ->references('id')
            ->on('user')->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('restaurant_id')
            ->references('id')
            ->on('restaurant')->onUpdate('cascade')->onDelete('cascade');
        });

        DB::statement('
            CREATE VIEW `restaurant_day_reserves_count` AS (
            SELECT DATE(user_reserve.`date`) AS `date`, SUM(user_reserve.`num_pers`) AS `count`, user_reserve.`restaurant_id`
            FROM user_reserve 
            WHERE user_reserve.`canceled` = 0 
            GROUP BY DATE(user_reserve.`date`), user_reserve.`restaurant_id`
        )');

        DB::statement('
            CREATE VIEW `restaurant_full_day` AS (
            SELECT t.`date`, sc.restaurant_id FROM `restaurant_scheduler` AS sc
            INNER JOIN `restaurant_day_reserves_count` AS t ON t.restaurant_id = sc.restaurant_id
            WHERE
            t.`count`*sc.`average_time` >= (
            SELECT SUM(ROUND(TIME_TO_SEC(TIMEDIFF(restaurant_scheduler_day.`hour_end`, restaurant_scheduler_day.`hour_start`))/60)) 
            FROM restaurant_scheduler_day 
            WHERE restaurant_scheduler_day.`scheduler_id`=sc.id
            AND restaurant_scheduler_day.`week_day`=DAYOFWEEK(t.date)-1 
            GROUP BY restaurant_scheduler_day.`week_day`
            )*sc.`capacity`
        )');

        DB::statement('
            CREATE VIEW `restaurant_hour_reserves_count` AS (
            SELECT ur.`restaurant_id`, DATE(ur.`date`) AS date, TIME(ur.`date`) AS time, SUM(ur.`num_pers`) AS count, SUM(IF(ur.`date_end` IS NULL, ur.`num_pers`, 0))+IFNULL(urs.`num_pers`, 0) AS pending_count, IF(SUM(IF(ur.`date_end` IS NULL, ur.`num_pers`, 0))+IFNULL(urs.`num_pers`, 0) >= sc.`capacity`, 1, 0) AS `is_full`
            FROM user_reserve AS ur INNER JOIN restaurant_scheduler AS sc ON (sc.restaurant_id = ur.restaurant_id AND (DATE(ur.date) > sc.`date_start` AND (DATE(ur.date) <= sc.`date_end` OR sc.`date_end` IS NULL))) LEFT JOIN user_reserve AS urs ON (ur.`restaurant_id` = urs.`restaurant_id` AND urs.`date_end` IS NULL AND urs.`date` >= DATE_SUB(ur.`date`, INTERVAL sc.`average_time` MINUTE) AND urs.`date` < ur.`date` AND urs.canceled = 0)
            WHERE ur.canceled = 0
            GROUP BY ur.`date`, ur.`restaurant_id`
        )');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_reserve');

        DB::statement('DROP VIEW `restaurant_day_reserves_count`');
        DB::statement('DROP VIEW `restaurant_full_day`');
        DB::statement('DROP VIEW `restaurant_hour_reserves_count`');
    }
}
