<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Restaurant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->string('name');
            $table->string('address');
            $table->string('phone');
            $table->boolean('approbed')->default(0);
            $table->string('description');
            $table->integer('restaurant_type_id')->unsigned()->index();
            $table->integer('city_id')->unsigned()->index();

            $table->timestamps();
            
            $table->foreign('restaurant_type_id')
            ->references('id')
            ->on('restaurant_type')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->foreign('city_id')
            ->references('id')
            ->on('city')
            ->onUpdate('cascade')
            ->onDelete('cascade');
        });

        Schema::create('restaurant_pui', function (Blueprint $table) {
            $table->integer('restaurant_id')->unsigned()->primary();
            $table->string('name');
            $table->string('address');
            $table->string('phone');
            $table->string('description');
            $table->integer('city_id')->unsigned()->index();

            $table->timestamps();

            $table->foreign('restaurant_id')
            ->references('id')
            ->on('restaurant')
            ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('city_id')
            ->references('id')
            ->on('city')
            ->onUpdate('cascade')
            ->onDelete('cascade');
        });

        Schema::create('restaurant_manager', function (Blueprint $table) {
            $table->integer('restaurant_id')->unsigned();
            $table->integer('manager_id')->unsigned();
            $table->enum('role', array('A', 'W', 'S'));
            $table->string('activation')->nullable();

            $table->primary(['restaurant_id', 'manager_id'], 'restaurant_manager_primary');

            $table->foreign('restaurant_id')
            ->references('id')
            ->on('restaurant')
            ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('manager_id')
            ->references('id')
            ->on('manager')
            ->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('user_restaurant_favourite', function (Blueprint $table) {
            $table->integer('restaurant_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->timestamps();
            
            $table->primary(['restaurant_id', 'user_id'], 'user_restaurant_fav_primary');

            $table->foreign('restaurant_id')
            ->references('id')
            ->on('restaurant')
            ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('user_id')
            ->references('id')
            ->on('user')
            ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant');
        Schema::dropIfExists('restaurant_manager');
    }
}
