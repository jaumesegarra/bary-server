<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'as' => 'admin.'], function() {
	Route::get('', function(){
		return redirect()->route('admin.index');
	});

	Route::get('login', ['as' => 'show_login', 'uses' => 'Admin\AuthController@showLoginForm']);
	Route::post('login', ['as' => 'login', 'uses' => 'Admin\AuthController@login']);

	Route::get('new', ['as' => 'index', 'uses' => 'Admin\ApproveNewRestaurantsController@index']);
	Route::get('_ajax/restaurant_for_approbed/{restaurant_id}', ['as' => 'ajax.rta', 'uses' => 'Admin\ApproveNewRestaurantsController@get_restaurant']);
	Route::post('approve/{id}/new', ['as' => 'approve_restaurant', 'uses' => 'Admin\ApproveNewRestaurantsController@approve']);
	Route::post('deny/{id}/new', ['as' => 'deny_restaurant', 'uses' => 'Admin\ApproveNewRestaurantsController@deny']);

	Route::get('changes', ['as' => 'changes', 'uses' => 'Admin\ApproveRestaurantsChangesController@index']);
	Route::post('approve/{id}/changes', ['as' => 'approve_changes', 'uses' => 'Admin\ApproveRestaurantsChangesController@approve']);

	Route::post('logout', ['as' => 'logout', 'uses' => 'Admin\AuthController@logout']);
});

Route::group(['prefix' => 'c'], function() {
	Route::get('user/verify/{code}', 'VerifyUserController@verify');

	Route::get('user/reset/{token}', 'ResetPwdUserController@showResetForm')->name('c.user.reset');
	Route::post('user/reset', 'ResetPwdUserController@reset')->name('c.user.reset.post');

	Route::get('user/remail/{code}', 'ResetEmailUserController@index')->name('c.user.remail');
});

Route::group(['prefix' => 'manager'], function() {

	// Portada
	Route::get('/', ['as' => 'welcome', 'uses' => 'WelcomePageController@index']);

	// Auth
	Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
	Route::post('login', ['as' => 'login.post', 'uses' => 'Auth\LoginController@login']);
	Route::post('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);
	Route::group(['prefix' => 'password', 'as' => 'password.'], function() {
		Route::get('reset', ['as' => 'request', 'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm']);
		Route::post('email', ['as' => 'email', 'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail']);
		Route::get('reset/{token}', ['as' => 'reset.token', 'uses' => 'Auth\ResetPasswordController@showResetForm']);
		Route::post('reset', ['as' => 'reset', 'uses' => 'Auth\ResetPasswordController@reset']);
	});

	// Asistente de registro (Usuarios y Restaurantes)
	Route::group(['prefix' => 'assistant', 'as' => 'assistant.'], function() {
		Route::get('start', ['as' => 'start', 'uses' => 'Assistant\StartController@showRegistrationForm']);
		Route::post('start', ['as' => 'start.post', 'uses' => 'Assistant\StartController@register']);

		Route::get('create', ['as' => 'create', 'uses' => 'Assistant\CreateController@showRegistrationForm']);
		Route::post('create', ['as' => 'create.post', 'uses' => 'Assistant\CreateController@register']);

		// Peticiones ajax para jquery
		Route::group(['prefix' => '_ajax', 'as' => 'ajax.'], function() {
			Route::get('restaurantTags', ['as' => 'restaurant_tags', 'uses' => 'Assistant\CreateController@getRestaurantTags']);
		});
	});

	/* EMAIL TESTS
	Route::get('mail/view', function ()
	{
		return new App\Mail\UserWelcome();
	});
	*/

	// Administración
	Route::get('/home', 'HomeController@index')->name('home');

	Route::group(['prefix' => 'restaurant', 'as' => 'restaurant.'], function() {

		Route::post('{restaurant_id}/apr', ['as' => 'approve_request', 'uses' => 'RestaurantManage\HomeController@apr']);

		Route::get('{restaurant_id}', ['as' => 'index', 'uses' => 'RestaurantManage\HomeController@index']);

		Route::get('{restaurant_id}/reserves_in_day/{date}', ['as' => 'index.ajax.reserves_data', 'uses' => 'RestaurantManage\HomeController@ajax_reserves_in_day']);
		Route::get('{restaurant_id}/available_dates', ['as' => 'index.ajax.available_dates', 'uses' => 'RestaurantManage\HomeController@ajax_available_days']);
		
		Route::group(['prefix' => '{restaurant_id}/reserves', 'as' => 'reserves.'], function() {
			Route::get('', ['as' => 'index', 'uses' => 'RestaurantManage\ReservesController@index']);

			Route::group(['prefix' => '_ajax', 'as' => 'ajax.'], function() {
				Route::get('events', ['as' => 'get', 'uses' => 'RestaurantManage\ReservesController@ajax_get_events']);
				Route::post('create', ['as' => 'create', 'uses' => 'RestaurantManage\ReservesController@ajax_create_event']);
				Route::post('event/{event_code}/finalize', ['as' => 'finalize', 'uses' => 'RestaurantManage\ReservesController@ajax_finalize_event']);
				Route::post('event/{event_code}/cancel', ['as' => 'cancel', 'uses' => 'RestaurantManage\ReservesController@ajax_cancel_event']);

				Route::get('get_hours_of_date', ['as' => 'av_hours', 'uses' => 'RestaurantManage\ReservesController@ajax_get_hours']);
			});
		});

		Route::group(['prefix' => '{restaurant_id}/menu', 'as' => 'menu.'], function() {
			Route::get('', ['as' => 'index', 'uses' => 'RestaurantManage\MenuController@index']);
			Route::post('', ['as' => 'save', 'uses' => 'RestaurantManage\MenuController@save']);
		});

		Route::resource('{restaurant_id}/photos', 'RestaurantManage\PhotosController');

		Route::post('{restaurant_id}/schedulers/closed_day', ['as' => 'schedulers.closed_day.store', 'uses' => 'RestaurantManage\SchedulersController@closed_day_store']);
		Route::delete('{restaurant_id}/schedulers/closed_day/{cld_date}', ['as' => 'schedulers.closed_day.destroy', 'uses' => 'RestaurantManage\SchedulersController@closed_day_destroy']);
		Route::get('{restaurant_id}/schedulers/{sch_id}/organize', ['as' => 'schedulers.organize', 'uses' => 'RestaurantManage\SchedulersController@organize']);

		// Peticiones ajax para jquery
		Route::group(['prefix' => '{restaurant_id}/schedulers/{sch_id}/_ajax', 'as' => 'schedulers.ajax.'], function() {
			Route::post('sch_day/create', ['as' => 'sch_day.create', 'uses' => 'RestaurantManage\SchedulersController@ajax_create']);
			Route::post('sch_day/update', ['as' => 'sch_day.update', 'uses' => 'RestaurantManage\SchedulersController@ajax_update']);
			Route::post('sch_day/remove', ['as' => 'sch_day.remove', 'uses' => 'RestaurantManage\SchedulersController@ajax_remove']);
		});
		Route::resource('{restaurant_id}/schedulers', 'RestaurantManage\SchedulersController');

		Route::get('{restaurant_id}/statistics', ['as' => 'statistics', 'uses' => 'RestaurantManage\StatisticsController@index']);

		Route::group(['prefix' => '{restaurant_id}/settings', 'as' => 'settings.'], function() {
			Route::get('', ['as' => 'info', 'uses' => 'RestaurantManage\SettingsInfoController@index']);
			Route::put('', ['as' => 'info.save', 'uses' => 'RestaurantManage\SettingsInfoController@save']);

			Route::get('managers', ['as' => 'managers', 'uses' => 'RestaurantManage\SettingsManagersController@index']);
			Route::get('_ajax/users', ['as' => 'managers.ajax.get_users', 'uses' => 'RestaurantManage\SettingsManagersController@ajax_get_users']);
			Route::post('managers', ['as' => 'managers.save', 'uses' => 'RestaurantManage\SettingsManagersController@save']);
		});
		Route::get('c/{code}', ['as' => 'settings.managers.confirmation', 'uses' => 'RestaurantManage\SettingsManagersController@confirmation']);
	});
});