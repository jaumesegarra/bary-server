<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserWelcome extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('vendor.notifications.email', ['greeting' => 'Hola Pedro,', 'introLines' => ['El restaurante Di marco ha cancelado su reserva para el 30/03/2018 a las 20:00. Por favor ponte en contacto con ellos para más información:'], 'actionText' => 'Contactar', 'actionUrl' => 'http://sdsdds', 'level' => 'green', 'outroLines' => ['Esperamos que pueda solucionar el problema muy pronto.']]);
    }
}
