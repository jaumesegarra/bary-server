<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ResetPassword;
use App\Notifications\RestaurantRegistered;

class Manager extends Authenticatable
{   
    protected $table = 'manager';

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    	'password', 'remember_token',
    ];


    public static $ROLE = [
        'A' => 'Admin',
        'W' => 'Camarero',
        'S' => 'Analista'
    ];

    public function restaurants()
    {   
        return $this->belongsToMany('App\Restaurant', 'restaurant_manager')->withPivot('role', 'activation');
    }

    public function hasRoleAt($restaurantId)
    {   
        $role = null;

        $data = $this->restaurants()->whereNull('activation')->where(['id' => $restaurantId])->first();
        if($data != null)
            $role = $data->pivot->role;

        return $role;
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword('password.reset.token', $token));
    }

    public function sendRestaurantRegisteredWelcome($restaurant_id, $restaurant_name)
    {
        $this->notify(new RestaurantRegistered($restaurant_id, $restaurant_name));
    }
}