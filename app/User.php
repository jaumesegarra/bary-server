<?php

namespace App;

use Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

use App\Notifications\ResetPassword;
use Carbon\Carbon;

class User extends Authenticatable implements JWTSubject
{   
    protected $table = 'user';
    
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname', 'dni', 'genre', 'email', 'phone', 'password', 'is_verified',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'verification_code',
    ];

    /**
     * Get reserve records associated with the user.
     */
    public function reserves(){
        return $this->hasMany('App\UserReserve', 'user_id', 'id')->orderBy('date', 'DESC');
    }

    /**
     * Get favourite restaurants records associated with the user.
     */
    public function favourites(){
        return $this->belongsToMany('App\Restaurant', 'user_restaurant_favourite');
    }

    /**
     * Automatically creates hash for the user password.
     *
     * @param  string  $value
     * @return void
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword('c.user.reset', $token));
    }

    public function createResetPasswordToken(){
        $token = str_random(64);

        \DB::table(config('auth.passwords.users.table'))->insert([
            'email' => $this->email, 
            'token' => $token,
            'created_at' => Carbon::now()->toDateTimeString()
        ]);

        return $token;
    }
}
