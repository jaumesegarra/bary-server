<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestaurantSchedulerDay extends Model
{
	protected $table = 'restaurant_scheduler_day';
	public $timestamps = false;

	public static $WEEK_NAMES = ['D','L','M','X','J','V','S'];

	/**
     * Protected attributes that CANNOT be mass assigned.
     *
     * @var array
     */
	protected $guarded = [];

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
		'code', 'scheduler_id', 'week_day', 'hour_start', 'hour_end',
	];

	public function week_day_name(){
		return self::$WEEK_NAMES[$this->week_day];
	}
}
