<?php

namespace App\Http\Controllers;

use App\City;
use App\Restaurant;

class WelcomePageController extends Controller
{

    /**
     * Show the welcome page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cities = City::whereIn('id', Restaurant::where('approbed', '=', 1)->groupBy('city_id')->pluck('city_id'))->limit(10)->get();

        return view('welcome', ['cities' => $cities]);
    }
}
