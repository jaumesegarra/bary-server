<?php
namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Carbon\Carbon;
use App\User;

class ResetEmailUserController extends Controller
{
    /**
     * Reset email
     *
     * @param LoginRequest $request
     * @param JWTAuth $JWTAuth
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($code)
    {
    	$msg = 'Error intentando revocar el email. Por favor intentelo mas tarde.'; $type = 'error';
    	$obj = decrypt($code);

    	if(Carbon::createFromFormat('Y-m-d', $obj['date_end'])->gte(Carbon::today()) && !User::where(['email' => $obj['email']])->exists()){
    		$user = User::find($obj['id']);

    		$user->email = $obj['email'];

    		if($user->save()){
    			$msg = 'Email actualizado correctamente!'; 
    			$type = 'success';
    		}
    	}

    	return view('c.message', ['message' => $msg, 'type' => $type, 'redirect' => true]);
    }
}