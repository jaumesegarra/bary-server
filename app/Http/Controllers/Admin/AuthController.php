<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{	
    use AuthenticatesUsers;

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }

    protected function guard()
    {
        return Auth::guard('admin');
    }
    
    protected $redirectTo = 'admin/new';

    public function showLoginForm()
    {
        return view('admin.login');
    }

    public function username()
    {
        return 'email';
    }

    public function logout(){
        Auth::guard('admin')->logout();

        return redirect($this->redirectTo);
    }
}