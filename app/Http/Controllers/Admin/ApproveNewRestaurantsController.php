<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Restaurant;
use Illuminate\Support\Facades\DB;

use App\Api\V1\Resources\PhotoResource;
use App\Api\V1\Resources\TagResource;
use App\Api\V1\Resources\MenuSectionResource;

use App\Notifications\RestaurantApproved;
use App\Notifications\RestaurantDenied;

class ApproveNewRestaurantsController extends Controller
{	
	/**
     * Create a new controller instance.
     *
     * @return void
     */
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function index()
	{
		$restaurants = Restaurant::whereApprobed(2)->with(['photos', 'menu_items', 'schedulers_days'])->withCount(['photos', 'menu_items', 'schedulers_days'])->orderBy('schedulers_days_count', 'DESC')->orderBy('menu_items_count', 'DESC')->orderBy('photos_count', 'DESC')->paginate(5);

		return view('admin.index', ['restaurants' => $restaurants]);
	}

	public function get_restaurant($restaurant_id)
	{
		$restaurant = Restaurant::where(['id' => $restaurant_id, 'approbed' => 2])->first();

		if($restaurant !== null)
			return [
				'id' => $restaurant->id,
				'name' => $restaurant->name,
				'type' => $restaurant->type->name,
				'address' => $restaurant->address,
				'city' => $restaurant->city->name,
				'phone' => $restaurant->phone,
				'photos' => PhotoResource::collection($restaurant->photos),
				'description' => $restaurant->description,
				'tags' => TagResource::collection($restaurant->tags),
				'menu' => MenuSectionResource::collection($restaurant->menu),
				'hasScheduler' => $restaurant->schedulers_days()->count()
			];
		else return abort(404);
	}

	public function approve($restaurant_id)
	{
		$restaurant = Restaurant::find($restaurant_id);

		$restaurant->approbed = 1;
		$restaurant->save();

		$managers = $restaurant->managers()->where('role', 'A')->whereNull('activation')->get();
		foreach ($managers as $manager)
			$manager->notify(new RestaurantApproved($restaurant_id, $restaurant->name, $manager->name));

		return redirect()->route('admin.index');
	}

	public function deny($restaurant_id, Request $request)
	{	
		$data = $request->only(['reason']);

		$restaurant = Restaurant::find($restaurant_id);

		$restaurant->approbed = 0;
		$restaurant->save();
		
		$managers = $restaurant->managers()->where('role', 'A')->whereNull('activation')->get();
		foreach ($managers as $manager)
			$manager->notify(new RestaurantDenied($restaurant_id, $restaurant->name, $manager->name, $data['reason']));

		return redirect()->route('admin.index');
	}
}