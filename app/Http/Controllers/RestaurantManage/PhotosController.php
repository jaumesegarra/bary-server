<?php

namespace App\Http\Controllers\RestaurantManage;

use App\Http\Controllers\Controller;
use App\Restaurant;
use App\RestaurantPhoto;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;
use App\Utils\ImageRepository;

class PhotosController extends Controller
{   

    private $imageRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {   
        $this->middleware(['auth', 'role'], ['only' => ['index']]);
        $this->middleware(['auth', 'role:A'], ['except' => ['index']]);

        $this->imageRepository = new ImageRepository();
    }


    /**
     * Show the restaurant photos.
     *
     * @return HTML
     */
    public function index($restaurant_id)
    {	
    	$restaurant = Restaurant::with('photos')->find($restaurant_id);

    	return view('restaurant.photos.index', ['restaurant' => $restaurant]);
    }

    private function show_edit($restaurant_id, $photo_path)
    {
        $restaurant = Restaurant::find($restaurant_id);
        $photo = RestaurantPhoto::where(['path' => $photo_path, 'restaurant_id' => $restaurant_id])->first();

        if($photo)
            return view('restaurant.photos.edit', ['restaurant' => $restaurant, 'photo' => $photo]);
        else 
            return redirect()->route('restaurant.photos.index', [$restaurant_id]);
    }


    public function show($restaurant_id, $photo_path)
    {
        return $this->show_edit($restaurant_id, $photo_path);
    }

    /**
    * Edit a photo.
    * 
    * @return HTML
    */
    public function edit($restaurant_id, $photo_path)
    {
        return $this->show_edit($restaurant_id, $photo_path);
    }

    public function update($restaurant_id, $photo_path, Request $request)
    {
        $data = $request->only(['description', 'main']);

        $validator = Validator::make($data, [
            'description' => 'required|string|max:255'
        ])->validate();

        $photo = RestaurantPhoto::where(['path' => $photo_path, 'restaurant_id' => $restaurant_id]);
        if($photo->exists()){
            $photo_data = [
                'description' => $data['description']
            ];

            if(isset($data['main'])){
                RestaurantPhoto::where(['restaurant_id' => $restaurant_id, 'main' => 1])->update(['main' => 0]);

                $photo_data['main'] = 1;
            }

            $photo->update($photo_data);

            return redirect()->route('restaurant.photos.index', [$restaurant_id]);
        }
    }

    /**
    * Upload photos.
    * 
    * @return HTML
    */
    public function create($restaurant_id)
    {
        $restaurant = Restaurant::find($restaurant_id);

        return view('restaurant.photos.create', ['restaurant' => $restaurant]);
    }

    public function store($restaurant_id)
    {
    	$photo = Input::all();

        $photo['restaurant_id'] = $restaurant_id;

        $response = $this->imageRepository->upload($photo);

        return $response;
    }

    /**
     * Delete a photo
     * @param  [int] $restaurant_id
     * @param  [string] $photo_path
     */
    public function destroy($restaurant_id, $photo_path)
    {   
        $result = RestaurantPhoto::where('path', '=', $photo_path)->delete();

        if($result)
            $this->imageRepository->delete($photo_path);

        return redirect(route('restaurant.photos.index', $restaurant_id));
    }
}