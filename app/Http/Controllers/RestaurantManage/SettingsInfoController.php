<?php

namespace App\Http\Controllers\RestaurantManage;

use App\Http\Controllers\Controller;
use App\City;
use App\Restaurant;
use App\RestaurantTag;
use App\RestaurantChanges;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class SettingsInfoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    	$this->middleware(['auth', 'role:A']);
    }

    public function index($restaurant_id)
    {	
    	$restaurant = Restaurant::find($restaurant_id);

    	$restaurant_tags = implode(", ", $restaurant->tags->pluck('name')->toArray());

    	return view('restaurant.settings.info', ['restaurant' => $restaurant, 'cities' => City::all(), 'restaurant_tags' => $restaurant_tags]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    private function validator(array $data)
    {   
    	$validator = Validator::make($data, [
    		'name' => 'required|string|max:255',
    		'city_id' => 'integer|exists:city,id',
    		'phone' => 'required|regex:/^(\+)?[0-9]+(\ [0-9]+)?$/',
    		'address' => 'required|string|max:255',
    		'description' => 'required|string|max:255',
    		'tags' => [function ($attribute, $value, $fail) {
    			if (!empty(trim($value))){
    				$tags = RestaurantTag::stringTagsToArray($value);

    				if(count($tags) > 10)
    					$fail('Has introducido más de 10 etiquetas!');

    				if(count(array_unique($tags)) != count($tags))
    					$fail('No puedes introducir etiquetas duplicadas.');
    			}
    		}]
    	]);

    	return $validator;
    }

    private function save_tags($restaurant_id, $tags_)
    {	
    	DB::insert("DELETE FROM restaurant_restaurant_tag WHERE restaurant_id = $restaurant_id");

    	$tags = RestaurantTag::stringTagsToArray($tags_);

    	foreach ($tags as $tagWord) {
    		$tag = RestaurantTag::Where(['name' => $tagWord])->first();

    		if($tag === null){
    			$tag = new RestaurantTag();
    			$tag->name = $tagWord;
    			$tag->save();
    		}

    		DB::insert("INSERT INTO restaurant_restaurant_tag (restaurant_id, restaurant_tag_id) VALUES ($restaurant_id, $tag->id)");
    	}
    }

    public function save($restaurant_id, Request $request)
    {
    	$data = $request->only(['name', 'city_id', 'phone', 'address', 'description', 'tags']);

    	$validator = $this->validator($data)->validate();

    	$restaurant = Restaurant::find($restaurant_id);

        if($restaurant->approbed != 2){
        	if($restaurant->approbed == 0){
        		$restaurant->name = $data['name'];

        		if($data['city_id'] != '')
        			$restaurant->city_id = $data['city_id'];

        		$restaurant->phone = $data['phone'];
        		$restaurant->address = $data['address'];
        		$restaurant->description = $data['description'];

        		$restaurant->save();
        	}else if($restaurant->isApprobed()){
        		$rp = RestaurantChanges::find($restaurant_id);
    			
    			$changes = [];

    			if($restaurant->name != $data['name'])
    				$changes['name'] = $data['name'];

    			if($data['city_id'] != '' && $restaurant->city_id != $data['city_id'])
    				$changes['city_id'] = $data['city_ic'];

    			if($restaurant->phone != $data['phone'])
    				$changes['phone'] = $data['phone'];

    			if($restaurant->address != $data['address'])
    				$changes['address'] = $data['address'];

    			if($restaurant->description != $data['description'])
    				$changes['description'] = $data['description'];

    			if(count($changes) > 0){
    				if($rp !== null){
    					$rp->update($changes);
    				}else
    					RestaurantChanges::create(array_merge($changes, ['restaurant_id' => $restaurant_id]));
    			}
        	}

        	$restaurant_tags = implode(", ", $restaurant->tags->pluck('name')->toArray());
        	if(((!isset($data['tags'])||trim($data['tags']) == '') && $restaurant->tags()->count() > 0)||$restaurant_tags != $data['tags'])
        		$this->save_tags($restaurant_id, $data['tags']);
        }

    	return redirect()->route('restaurant.settings.info', [$restaurant_id]);
    }
}