<?php

namespace App\Http\Controllers\RestaurantManage;

use App\Http\Controllers\Controller;
use App\Restaurant;
use App\UserReserve;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Notifications\ReserveCanceled;
use App\RestaurantScheduler;

class ReservesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    	$this->middleware(['auth', 'role'], ['only' => ['index', 'ajax_get_events']]);

        $this->middleware(['auth', 'role:A,W'], ['except' => ['index', 'ajax_get_events']]);
    }

    /**
     * Show the restaurant manage dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {	
    	$restaurant = Restaurant::find($id);

    	return view('restaurant.reserves.index', ['restaurant' => $restaurant]);
    }

    /**
     * Get reserves of a day
     *
     * @return \Illuminate\Http\Response
     */
    public function ajax_get_events($restaurant_id, Request $request)
    {   
        $restaurant = Restaurant::find($restaurant_id);

        $data = $request->only(['start', 'end']);
        $events = [];

        $reserves = $restaurant->reserves($data['start'])->get();
        foreach ($reserves as $reserve) {
            $reserve_event = [
                'id' => $reserve->code,
                'title' => $reserve->name(),
                'start' => $reserve->date->toDateTimeString(),
                'num_pers' => $reserve->num_pers
            ];

            if($reserve->suggestions)
                $reserve_event['description'] = $reserve->suggestions;
            
            if($reserve->date_end)
                $reserve_event['end'] = $reserve->date_end->toDateTimeString();

            if($reserve->canceled)
                $reserve_event['canceled'] = 1;

            $events[] = $reserve_event;
        }

        return response()->json($events,200);
    }

    /**
     * Create a reserve
     *
     * @return \Illuminate\Http\Response
     */
    public function ajax_create_event($restaurant_id, Request $request)
    {
        $data = $request->only(['name', 'suggestions', 'date', 'hour', 'num_pers']);

        $reserve = new UserReserve();
        $reserve->name = $data['name'];
        if(isset($data['suggestions']))
            $reserve->suggestions = $data['suggestions'];
        $reserve->restaurant_id = $restaurant_id;
        $reserve->date = Carbon::createFromFormat('d/m/Y H:i:s', $data['date'].' '.$data['hour'].':00');
        $reserve->num_pers = $data['num_pers'];
        $reserve->generateCode();

        $result = $reserve->save();

        return response()->json(['status' => (($result) ? 1 : 0)], (($result) ? 200 : 500));
    }

    /**
     * Finalize a reserve
     *
     * @return \Illuminate\Http\Response
     */
    public function ajax_finalize_event($restaurant_id, $reserve_code)
    {
        $restaurant = Restaurant::find($restaurant_id);

        $reserve = $restaurant->reserves()->where(['code' => $reserve_code]);
        if($reserve->exists()){
            $reserve_data = $reserve->first();
            if($reserve_data->date->lte(Carbon::now()) && !$reserve_data->date_end){
                $reserve->update(['date_end' => Carbon::now()->toDateTimeString()]);

                return response()->json(['status' => 1], 200);
            }else return response()->json(['status' => 0], 500);
        }
    }

    /**
     * Cancel a reserve
     *
     * @return \Illuminate\Http\Response
     */
    public function ajax_cancel_event($restaurant_id, $reserve_code)
    {
        $restaurant = Restaurant::find($restaurant_id);

        $reserve = $restaurant->reserves()->where(['code' => $reserve_code]);
        if($reserve->exists()){
            $reserve_data = $reserve->first();
            if(!$reserve_data->isExpired()){
                $reserve->update(['canceled' => 1]);

                if($reserve_data->user){
                    try{
                        $reserve_data->user->notify(new ReserveCanceled($reserve_data));
                    }catch(\Exception $exception){}
                }

                return response()->json(['status' => 1], 200);
            }else return response()->json(['status' => 0], 500);
        }
    }

    /**
     * Get available hours from a date
     *
     * @return \Illuminate\Http\Response
     */
    public function ajax_get_hours($restaurant_id, Request $request)
    {
        $data = $request->only(['date']);
        $restaurant = Restaurant::find($restaurant_id);
        $hours = [];

        if($restaurant !== null && isset($data['date'])){
            $date = Carbon::createFromFormat('d/m/Y', $data['date']);
            if($date !== null)
                $hours = $restaurant->available_hours($date, true);
        }

        return response()->json(['hours' => $hours], 200);
    }
}