<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{	
	protected $table = 'city';

	/**
     * Protected attributes that CANNOT be mass assigned.
     *
     * @var array
     */
	protected $guarded = ['id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'name', 'country',
    ];

    public function restaurants()
    {
        return $this->hasMany('App\Restaurant', 'city_id','id');
    }

    public function random_image()
    {   
        $photo = null;

        $restaurantwphoto = $this->restaurants()->inRandomOrder()->has('photos')->first();
        if($restaurantwphoto != null)
            $photo = $restaurantwphoto->photos()->where(['main' => 1])->first()->getThumbUrl();

        return $photo;
    }
}
