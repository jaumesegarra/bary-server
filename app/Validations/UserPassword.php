<?php

namespace App\Validations;

use Illuminate\Contracts\Validation\Rule;

use Hash;
use Auth;

class UserPassword implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {   
        $user = Auth::guard()->user();
        
        return Hash::check($value, $user->password);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Contraseña incorrecta!';
    }
}