<?php

namespace App\Validations;

use Illuminate\Contracts\Validation\Rule;
use App\Restaurant;
use Carbon\Carbon;

class ValidReserveDate implements Rule
{   
    private $restaurant_id;
    private $date;
    private $num_pers;

    public function __construct($restaurant_id, $date, $num_pers = null)
    {
        $this->restaurant_id = $restaurant_id;
        $this->date = $date;
        $this->num_pers = $num_pers;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {       
        $result = false;

        $date = new Carbon($this->date.' '.$value.':00');

        $restaurant = Restaurant::find($this->restaurant_id);
        if($restaurant !== null)
            $result = $restaurant->check_valid_reserveDate($date, $this->num_pers);

        return $result;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Fecha y/o hora no disponibles.';
    }
}