<?php

namespace App\Utils;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Gumlet\ImageResize;

use App\RestaurantPhoto;

class ImageRepository
{
    public function upload($form_data)
    {
        $validator = Validator::make($form_data, [
            'qqfile' => 'required|image|mimes:jpeg,png,jpg|max:3000',
            'description' => 'required|string'
        ]);

        if ($validator->fails()) {
            return Response::json([
                'error' => true,
                'message' => $validator->messages()->first(),
                'code' => 400
            ], 400);
        }

        $photo = $form_data['qqfile'];
        $originalName = $form_data['qqfilename'];

        $on_split = preg_split('/[.]/', $originalName);
        $extension = $on_split[1];
        $filename = $on_split[0];

        $path = $this->createUniqueFilename($filename, $extension);
        $allowed_filename = $path.'.'.$extension;

        $moveToUploadFolder = ($photo->move(public_path(Config::get('images.full_size')), $allowed_filename));
        $createThumbPhoto = $this->createThumb($photo, $allowed_filename);

        if(!$moveToUploadFolder || !$createThumbPhoto){
            return Response::json([
                'success' => false,
                'message' => 'Server error while uploading',
                'code' => 500
            ], 400);
        }

        $restaurant_photos = RestaurantPhoto::where(['restaurant_id' => $form_data['restaurant_id']])->count();
        $isMain = ($restaurant_photos == 0) ? 1 : 0;

        RestaurantPhoto::create([
            'path' => $path,
            'format' => $extension,
            'description' => $form_data['description'],
            'restaurant_id' => $form_data['restaurant_id'],
            'main' => $isMain
        ]);

        return Response::json([
            'code'  => 200,
            'success' => true
        ], 200);
    }

    public function createUniqueFilename($filename, $extension)
    {
        return md5($filename).substr(sha1(mt_rand().$extension), 0, 5);
    }

    /**
     * Create Thumb From Original
     */
    public function createThumb($photo, $filename)
    {       
        $image = new ImageResize(public_path(Config::get('images.full_size')).$filename);
        $image->crop(320, 240);
        $image->save(public_path(Config::get('images.icon_size')).$filename);

        return $image;
    }

    /**
     * Delete Image From Session folder, based on original filename
     */
    public function delete($originalFilename)
    {

        $full_path1 = Config::get('images.full_size').$originalFilename;
        $full_path2 = Config::get('images.icon_size').$originalFilename;

        if (File::exists($full_path1))
            File::delete($full_path1);
            
        if (File::exists($full_path2))
            File::delete($full_path2);

        return true;
    }


    function sanitize($string, $force_lowercase = true, $anal = false) {
        $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
                       "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
                       "â€”", "â€“", ",", "<", ".", ">", "/", "?");
        $clean = trim(str_replace($strip, "", strip_tags($string)));
        $clean = preg_replace('/\s+/', "-", $clean);
        $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;

        return ($force_lowercase) ?
        (function_exists('mb_strtolower')) ?
        mb_strtolower($clean, 'UTF-8') :
        strtolower($clean) :
        $clean;
    }
}