<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class UserReserve extends Model
{
	protected $table = 'user_reserve';
	protected $primaryKey = 'code';
	protected $keyType = 'string';
	public $incrementing = false;
	
	public function generateCode()
	{
		$this->code = preg_replace('|/|', '', Hash::make($this->date->toDateTimeString().Carbon::now()->toDateTimeString().md5(microtime())));;
	}

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
		'code', 'user_id', 'restaurant_id', 'date', 'date_end', 'suggestions', 'canceled', 'name',
	];

	protected $dates = ['date', 'date_end'];
	
	public function name()
	{
		return (($this->user) ? $this->user->name.' '.$this->user->surname : $this->name);
	}

	public function isCanceled()
	{
		return ($this->canceled == 1);
	}

	public function hasAssist()
	{	
		return ($this->date_end !== null);
	}

	public function state()
	{
		return ($this->hasAssist()) ? 'ASSISTED' : (($this->isCanceled()) ? 'CANCELED' : 'NOT ASSISTED');
	}

	public function isExpired()
	{
		return ($this->date->lt(Carbon::now()) || $this->date_end);
	}

	/**
     * Get the user record associated with the reserve.
     */
	public function user()
	{	
		return $this->hasOne('App\User', 'id','user_id');
	}

	/**
     * Get the restaurant record associated with the reserve.
     */
	public function restaurant()
	{	
		return $this->hasOne('App\Restaurant', 'id','restaurant_id');
	}
}
