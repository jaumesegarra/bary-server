<?php

namespace App;

use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model;

class RestaurantPhoto extends Model
{
	protected $table = 'restaurant_photo';

    private static function getURL()
    {
      return Config::get('app.url').Config::get('images.route_path');
  }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'path', 'format', 'description', 'restaurant_id', 'main'
    ];

    public function getThumbUrl(){
    	return self::getUrl().'thumb/'.$this->path.'.'.$this->format;
    }

    public function getOriginalUrl(){
    	return self::getUrl().'original/'.$this->path.'.'.$this->format;
    }

    public function isMain()
    {
        return ($this->main == 1);
    }
}
