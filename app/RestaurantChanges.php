<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestaurantChanges extends Model
{	
	protected $table = 'restaurant_pui';
    protected $primaryKey = 'restaurant_id';
    public $incrementing = false;
    
	/**
     * Protected attributes that CANNOT be mass assigned.
     *
     * @var array
     */
	protected $guarded = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'restaurant_id', 'name', 'address', 'phone', 'description', 'city_id', 
    ];

    /**
     * Get the city record associated with the restaurant.
     */
    public function city()
    {   
        return $this->hasOne('App\City', 'id','city_id');
    }
}
