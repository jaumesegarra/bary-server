<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestaurantClosedDay extends Model
{
	protected $table = 'restaurant_closed_day';
	public $timestamps = false;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
		'restaurant_id', 'date',
	];

	protected $dates = ['date'];
}
