<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use Auth;

class ReserveRegistered extends Notification
{
	use Queueable;

	private $name;
	private $num_pers;
    private $date;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($name, $num_pers, $date)
    {   
    	$this->name = $name;
    	$this->num_pers = $num_pers;
        $this->date = $date;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
    	return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {	
    	$user = Auth::guard()->user();

    	return (new MailMessage)
    	->subject('Gracias por tu reserva!')
    	->greeting('Hola '.$user->name.'!')
    	->line('Acabas de reservar mesa para '.$this->num_pers.' personas en '.$this->name.' el dia '.$this->reserve->date->format('d/m/Y').' a las '.$this->reserve->date->format('H:i').'. Puede ver más información sobre sus reservas desde nuestra web:')
    	->action('Ver reservas', url('reserves'))
    	->line('Recuerde opinar sobre el restaurante una vez finalizada su estancia en él :)');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
    	return [
            //
    	];
    }
}