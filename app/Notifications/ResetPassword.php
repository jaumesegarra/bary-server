<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ResetPassword extends Notification
{
    use Queueable;

    private $url;
    private $code;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($url, $code)
    {   
        $this->url=$url;
        $this->code=$code;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
        ->subject('¿Olvidó la contraseña?')
        ->line('Está recibiendo este mensaje por que hemos recibido una solicitud para el cambio de contraseña de su cuenta.')
        ->line('Haga click en el botón de abajo si desea cambiar su contraseña:')
        ->action('Cambiar contraseña', url(route($this->url, [$this->code])))
        ->line('Si no has solicitado el cambio, por favor ignora este mensaje.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
