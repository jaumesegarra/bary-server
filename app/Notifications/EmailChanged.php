<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class EmailChanged extends Notification
{
    use Queueable;

    private $reset_code;
    private $email;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($reset_code, $email)
    {   
        $this->reset_code = $reset_code;
        $this->email = $email;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
        ->subject('Su correo eléctronico ha sido actualizado!')
        ->line("Está recibiendo este mensaje por que acaba de actualizar su email de Bary, ahora es '$this->email'.")
        ->line('Si usted no ha realizado dicho cambio, haga click en el botón de abajo para resetear los cambios:')
        ->action('Revocar', url(route('c.user.remail', [$this->reset_code])))
        ->line('Tiene 48H para revocar el cambio.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
