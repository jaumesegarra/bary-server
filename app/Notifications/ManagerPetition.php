<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use App\Manager;

class ManagerPetition extends Notification
{
    use Queueable;

    private $restaurant_name;
    private $role;
    private $code;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($restaurant_name, $role, $code)
    {   
        $this->restaurant_name = $restaurant_name;
        $this->role = $role;
        $this->code = $code;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
        ->subject("Permiso para administrar el restaurante $this->restaurant_name !")
        ->line("Estimado administrador.")
        ->line("Está recibiendo este mensaje por que alguien le ha otorgado los permisos de ".Manager::$ROLE[$this->role]." en el restaurante $this->restaurant_name.")
        ->line('Haga click en el botón de abajo para confirmar:')
        ->action('Confirmar', url(route('restaurant.settings.managers.confirmation', [$this->code])))
        ->line('Si usted no es el posible destinatario de este correo por favor ignore-lo');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}