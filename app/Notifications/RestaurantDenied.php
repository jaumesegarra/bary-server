<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class RestaurantDenied extends Notification
{
    use Queueable;

    private $restaurant_id;
    private $restaurant_name;
    private $name;
    private $reason;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($restaurant_id, $restaurant_name, $name, $reason)
    {   
        $this->restaurant_id = $restaurant_id;
        $this->restaurant_name = $restaurant_name;
        $this->name = $name;
        $this->reason = $reason;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
        ->subject('Su solicitud de aprobación ha sido rechazada')
        ->greeting('Hola '.$this->name.'!')
        ->line('Su solicitud de aprobación para el restaurante '.$this->restaurant_name.' ha sido rechazada debido al siguiente motivo:')
        ->line('"'.$this->reason.'"')
        ->line('Modifique la información oportuna para solicitar de nuevo su aprobación.')
        ->action('Administrar', url(route('restaurant.index', [$this->restaurant_id])))
        ->line('Esperamos que se solucione pronto este problema y su restaurante forme parte de nuestra comunidad.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
