<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use Carbon\Carbon;

class ReserveCanceled extends Notification
{
	use Queueable;

	private $reserve;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($reserve)
    {   
    	$this->reserve = $reserve;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
    	return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {   
        Carbon::setLocale('es');
        setlocale(LC_TIME, 'Spanish');
        Carbon::setUtf8(true);
        
        return (new MailMessage)
        ->subject('Reserva cancelada...')
        ->greeting('Hola '.$this->reserve->user->name.'!')
        ->line('El restaurante '.$this->reserve->restaurant->name.' ha cancelado su reserva para el '.$this->reserve->date->format('d/m/Y').' a las '.$this->reserve->date->format('H:i').'. Por favor ponte en contacto con ellos para más información:')
        ->action('Contactar', 'tel:'.$this->reserve->restaurant->phone)
        ->line('Esperamos que pueda solucionar el problema muy pronto.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
    	return [
            //
    	];
    }
}