<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class Verify extends Notification
{
    use Queueable;

    private $name;
    private $verification_code;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($name, $verification_code)
    {   
        $this->name = $name;
        $this->verification_code = $verification_code;   
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
        ->subject('Bienvenido/a a Bary!')
        ->greeting('Hola '.$this->name.'!')
        ->line('Gracias por registrarte en nuestra comunidad. Por favor haz click en el botón de abajo para verificar tu correo:')
        ->action('Verificar', url('c/user/verify', $this->verification_code))
        ->line('Si ha recibido el email por error, por favor ignorelo.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
