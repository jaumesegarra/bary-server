<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class RestaurantRegistered extends Notification
{
    use Queueable;

    private $restaurant_id;
    private $restaurant_name;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($restaurant_id, $restaurant_name)
    {   
        $this->restaurant_id = $restaurant_id;
        $this->restaurant_name = $restaurant_name;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
        ->subject('Su restaurante ha sido registrado con éxito!')
        ->greeting('Hola '.\Auth::user()->name.'!')
        ->line('Su restaurante '.$this->restaurant_name.' esta a un paso de ser parte de esta gran comunidad, añada más información sobre él (como la carta del restaurante, el horario de reservas, imagenes del local y de la comida, etc...) y solicite su aprobación desde nuestro panel.')
        ->action('Administrar', url(route('restaurant.index', [$this->restaurant_id])))
        ->line('Una vez solicitada su aprobación solo deberá esperar a que su restaurante sea visible para el resto de usuarios.')
        ->line('Cuanta más información añada más rápido será el proceso de aprobación.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
