<?php

namespace App\Api\V1\Resources;

use Illuminate\Http\Resources\Json\Resource;

class MenuItemResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {      
        $section = $this->section;
        $restaurant = $section->restaurant;

        return [
            'restaurant' => [
                'id' => $restaurant->id,
                'name' => $restaurant->name
            ],
            'section' => [
                'icon' => $section->icon,
                'title' => $section->title
            ],
            'name' => $this->name,
            'description' => $this->description,
            'price' => $this->price
        ];
    }
}