<?php

namespace App\Api\V1\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Api\V1\Resources\RestaurantCardResource;

class ReserveResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
    	return [
            'code' => $this->code,
            'date' => $this->date->toDateTimeString(),
            'num_pers' => $this->num_pers,
            'restaurant' => new RestaurantCardResource($this->restaurant),
            'state' => $this->state()
        ];
    }
}