<?php

namespace App\Api\V1\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Api\V1\Resources\UserSmallResource;

class ReviewResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
    	return [
            'rate' => $this->rate,
            'title' => $this->title,
            'description' => $this->description,
            'user' => new UserSmallResource($this->user),
            'date' => ($this->updated_at) ? $this->updated_at->toDateTimeString() : null
        ];
    }
}