<?php

namespace App\Api\V1\Resources;

use Auth;
use App\Api\V1\Resources\PhotoResource;
use App\Api\V1\Resources\TagResource;
use App\Api\V1\Resources\CityResource;
use App\Api\V1\Resources\MenuSectionResource;
use App\Api\V1\Resources\ReviewResource;
use Illuminate\Http\Resources\Json\Resource;

class RestaurantResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {   
        return [
            'id' => $this->id,
            'name' => $this->name,
            'type' => $this->type->name,
            'rate' => $this->rate(),
            'address' => $this->address,
            'city' => new CityResource($this->city),
            'phone' => $this->phone,
            'photos' => PhotoResource::collection($this->photos),
            'description' => $this->description,
            'tags' => TagResource::collection($this->tags),
            'menu' => [
                'average_price' => $this->average_price(), 
                'sections' => MenuSectionResource::collection($this->menu)
            ],
            'reviews' => [
                'count' => [
                    'total' => $this->reviews->count(),
                    'stars' => $this->rate_count_by_stars()
                ],
                'me' => (Auth::check()) ? new ReviewResource($this->reviews()->with('user')->where('user_id', '=', Auth::id())->first()) : null,
                'recents' => ReviewResource::collection($this->reviews()->with('user')->where(function ($q) {
                    $q->whereNotNull('title');

                    if(Auth::check())
                        $q->where('user_id', '!=', Auth::id());
                    })->take(5)->get())
            ],
            'was_client' => $this->was_client(),
            'is_favourite' => $this->is_favourite(),
            'available_days' => (Auth::check()) ? $this->available_days() : false
        ];
    }
}