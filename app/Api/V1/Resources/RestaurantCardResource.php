<?php

namespace App\Api\V1\Resources;

use App\Api\V1\Resources\PhotoResource;
use Illuminate\Http\Resources\Json\Resource;

class RestaurantCardResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'type' => $this->type->name,
            'rate' => $this->rate(),
            'address' => $this->address,
            'image' => new PhotoResource($this->photos->first()),
            'average_price' => $this->average_price()
        ];
    }
}