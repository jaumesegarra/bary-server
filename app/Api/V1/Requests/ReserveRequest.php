<?php

namespace App\Api\V1\Requests;

use Illuminate\Http\Request;
use Dingo\Api\Http\FormRequest;
use App\Validations\ValidReserveDate;

class ReserveRequest extends FormRequest
{	
	private $restaurant_id;
	private $date;

	public function __construct(Request $request)
	{
		$this->restaurant_id = $request->restaurant_id;
		$this->date = $request->date;
	}

	public function rules()
	{
		return [
			'hour' => ['required', 'regex:/^([0-2][0-9]:[0-5][0-9])$/', new ValidReserveDate($this->restaurant_id, $this->date, $this->num_pers)],
			'num_pers' => 'required|integer|min:1|max:20',
			'suggestions' => 'max:255'
		];
	}

	public function authorize()
	{
		return true;
	}
}
