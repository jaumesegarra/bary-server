<?php

namespace App\Api\V1\Requests;

use Dingo\Api\Http\FormRequest;
use Auth;
use App\User;

class UserDataRequest extends FormRequest
{	
	public static $GLOBAL_RULES = [
		'name' => 'required|max:255',
		'surname' => 'required|max:255',
		'genre' => 'required|in:M,F'
	];

	public function rules()
	{	
		$arr = array_merge(self::$GLOBAL_RULES, AdditionalFieldsRequest::$GLOBAL_RULES);
		$arr['dni'][] = new \App\Validations\DniLetter;
		
		return $arr;
	}

	public function authorize()
	{
		return true;
	}
}