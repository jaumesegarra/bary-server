<?php

namespace App\Api\V1\Requests;

use Dingo\Api\Http\FormRequest;

class AdditionalFieldsRequest extends FormRequest
{	

	public static $GLOBAL_RULES = [
		'dni' => ['regex:/^(([X-Z]{1})([-]?)(\d{7})([-]?)([A-Z]{1}))|((\d{8})([-]?)([A-Z]{1}))$/'],
		'phone' => ['min:9', 'regex:/^(\+)?[0-9]+(\ [0-9]+)?$/']
	];

	public function rules()
	{
		return [
			'dni' => array_merge(['required_without:phone'], self::$GLOBAL_RULES['dni']),
			'phone' => array_merge(['required_without:dni'], self::$GLOBAL_RULES['phone'])
		];
	}

	public function authorize()
	{
		return true;
	}
}
