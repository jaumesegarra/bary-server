<?php

namespace App\Api\V1\Controllers;

use App\User;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\SignUpRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;

use App\Notifications\Verify;

class SignUpController extends Controller
{
    public function signUp(SignUpRequest $request, JWTAuth $JWTAuth)
    {

        $user = new User($request->all());
        $user->verification_code = str_random(30);

        if(!$user->save()) {
            throw new HttpException(500);
        }
        
        try{
            $user->notify(new Verify($request->name, $user->verification_code));
        }catch(\Exception $e){};
        
        $token = $JWTAuth->fromUser($user);
        return response()->json([
            'status' => 1,
            'token' => $token
        ], 201);
    }
}
