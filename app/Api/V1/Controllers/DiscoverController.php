<?php

namespace App\Api\V1\Controllers;

use App\Http\Controllers\Controller;
use App\Restaurant;
use App\Api\V1\Resources\RestaurantCardResource;
use Auth;

class DiscoverController extends Controller
{

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct(){}


    /**
     * Get random restaurants
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($city_id)
    {
    	$restaurants = Restaurant::inRandomOrder()->where(['city_id' => $city_id, 'approbed' => 1])->take(8)->get();

    	return response()->json(RestaurantCardResource::collection($restaurants));
    }
}
?>