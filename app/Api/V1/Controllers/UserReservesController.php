<?php

namespace App\Api\V1\Controllers;

use App\Http\Controllers\Controller;

use Auth;
use App\UserReserve;
use App\RestaurantScheduler;
use App\Api\V1\Resources\ReserveResource;
use App\Api\V1\Requests\EditReserveRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UserReservesController extends Controller
{

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {   
    	$this->middleware('auth:api');
    }

    /**
     * Get all user reserves
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {   
        $data = $request->only(['p']);

        $results_x_page = 10;
        $page = (isset($data['p']) ? $data['p'] : 1);

        $user = Auth::guard()->user();

        $coming_reserves = $user->reserves()->whereRaw('DATE(NOW()) <= DATE(`date`)')->whereNull('date_end')->where(['canceled' => 0])->get();

        $others_reserves = $user->reserves()->whereDate('date', '<', Carbon::today())->orWhereNotNull('date_end')->orWhere(['canceled' => 1]);


        $total = $others_reserves->count();
        $num_pages = ceil($total/$results_x_page);

        return response()->json([
            'coming' => ReserveResource::collection($coming_reserves),
            'others' => [
                'data' => ReserveResource::collection($others_reserves->forPage($page, $results_x_page)->get()),
                'pages' => $num_pages,
                'total' => $total
            ]
        ], 200);
    }

    /**
     * Cancel a reserve
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancel($reserve_code)
    {
    	$reserve = UserReserve::find($reserve_code);
    	
    	if($reserve != null && !$reserve->isExpired() && $reserve->user->id == Auth::id()){

    		$process = $reserve->update(['canceled' => 1]);
    		if($process)
    			return response()->json(['status' => 1], 200);
    	}

    	return response()->json(['status' => 0], 501);
    }

    /**
     * Edit a reserve
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($reserve_code, EditReserveRequest $request)
    {	
    	$data = $request->only(['num_pers_additional']);
    	$status = 0;

    	$reserve = UserReserve::find($reserve_code);

    	if($reserve != null && !$reserve->isExpired() && !$reserve->isCanceled() && $reserve->user->id == Auth::id()){
    		if(isset($data['num_pers_additional']))
    			$reserve->num_pers = $reserve->num_pers + $data['num_pers_additional'];

            $diff_time = 0;
            $reserve_time = 0;


            if($data['num_pers_additional'] <= 0 || $reserve->restaurant->check_valid_reserveDate($reserve->date, $data['num_pers_additional'])){

               if($reserve->save()) $status = 1;

           }else $status = -1;
       }

       return response()->json(['status' => $status], (($status == 1) ? 200 : 501));
   }
}