<?php

namespace App\Api\V1\Controllers;

use App\Http\Controllers\Controller;

use Auth;
use App\Api\V1\Resources\RestaurantCardResource;
use Illuminate\Http\Request;

class UserFavouritesController extends Controller
{

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {   
    	$this->middleware('auth:api');
    }

    /**
     * Get favourite restaurants of the user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {      
    	$data = $request->only(['p']);

    	$results_x_page = 10;
    	$page = (isset($data['p']) ? $data['p'] : 1);

    	$user = Auth::guard()->user();

    	$favourites = $user->favourites();

    	$total = $favourites->count();
    	$num_pages = ceil($total/$results_x_page);

    	$response = [
    		'data' => RestaurantCardResource::collection($favourites->forPage($page, $results_x_page)->get()),
    		'pages' => $num_pages,
    		'total' => $total
    	];

    	return response()->json($response);
    }
}
