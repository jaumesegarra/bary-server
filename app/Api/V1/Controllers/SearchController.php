<?php
namespace App\Api\V1\Controllers;

use App\Http\Controllers\Controller;
use App\Restaurant;
use App\RestaurantClosedDay;
use App\RestaurantScheduler;
use App\UserReserve;
use App\RestaurantMenuSection;
use App\RestaurantMenuItem;
use Carbon\Carbon; use Illuminate\Support\Facades\DB;
use App\Api\V1\Resources\MenuItemResource;
use App\Api\V1\Resources\RestaurantCardResource;
use App\Api\V1\Resources\RestaurantSmallResource;
use App\Api\V1\Requests\SearchRequest;
use App\RestaurantType;
use App\RestaurantTag;

class SearchController extends Controller
{

    /**
     * Create a new instance.
     *
     * @return void
     */
    public function __construct(){}

    /**
     * Get suggestions
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function outstading($city_id)
    {   
        $tags = RestaurantTag::whereIn('id', DB::table('restaurant_restaurant_tag')->inRandomOrder()->groupBy('restaurant_tag_id')->whereIn('restaurant_id', Restaurant::inRandomOrder()->where(['city_id' => $city_id, 'approbed' => 1])->take(5)->pluck('id'))->take(5)->pluck('restaurant_tag_id'))->pluck('name');
        
        return response()->json($tags);
    }

    /**
     * Get restaurant and tags suggestions
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function term($city_id, $q) {
        $q = strtolower($q);

        $response = [
            'restaurants' => [],
            'tags' => []
        ];

        $restaurants_query = Restaurant::where(['city_id' => $city_id, 'approbed' => 1])->search("*$q*", ['name','address','description'])->take(5);

        $response['restaurants'] = RestaurantSmallResource::collection($restaurants_query->get());

        $types_query = RestaurantType::whereRaw("LOWER(name) LIKE '%".$q."%'")
        ->whereIn('id', Restaurant::where('city_id', '=', $city_id)->pluck('restaurant_type_id'))
        ->take(1);
        foreach ($types_query->get() as $type)
            $response['tags'][] = $type->name;

        $city_tags = DB::table('restaurant_restaurant_tag')->whereIn('restaurant_id', Restaurant::where('city_id', '=', $city_id)->pluck('id'))->groupBy('restaurant_tag_id')->pluck('restaurant_tag_id');

        $tags_query = RestaurantTag::whereRaw("LOWER(name) LIKE '%".$q."%'")
        ->whereIn('id', $city_tags)
        ->take(1);
        foreach ($tags_query->get() as $tag)
            $response['tags'][] = $tag->name;

        return response()->json($response);
    }

    /**
     * Get search results
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function search($city_id, SearchRequest $request) {
        $data = $request->only(['p', 'q', 'date', 'num_pers']);
        $results = [
            'eat' => [],
            'restaurants' => [
                'data' => [],
                'pages' => 0
            ]
        ];

        $results_x_page = 10;
        $num_page = 1;
        if(isset($data['p']))
            $num_page = $data['p'];

        $restaurants_json = [];

        $restaurants = Restaurant::where(['city_id' => $city_id, 'approbed' => 1]);

        if(isset($data['date']) && isset($data['num_pers'])){
            $date = new Carbon($data['date']);
            $num_pers = $data['num_pers'];

            $day = new Carbon($date->toDateString());
            $today = new Carbon(date('Y-m-d'));

            if($day->gte($today)){
                $hour_str = $date->format('H:i:s');
                $date_str = $date->toDateString();
                $day_week = $date->dayOfWeek;

                $restaurants
                ->whereNotIn('id', RestaurantClosedDay::where(['date' => $date_str])->pluck('restaurant_id')) //Quitar restaurantes cerrados ese dia de la lista
                ->whereIn('id', RestaurantScheduler::whereDate('date_start', '<=', $date)->where(function ($q) use ($date){ 
                    $q->whereDate('date_end', '>=', $date)->orWhereNull('date_end');
                })->get()->filter(function ($sch) use ($date, $date_str, $hour_str, $day_week, $num_pers){
                    $isOpen = $sch->days()->whereTime('hour_start', '<=', $hour_str)->whereTime('hour_end', '>=', $hour_str)->where('week_day', $day_week)->exists(); //Quitar restaurantes cerrados ese hora de la lista
                    $isFull = false;
                    if($isOpen){ //Quitar restaurantes completos esa hora de la lista
                        $rsh = DB::select(DB::raw('SELECT pending_count FROM restaurant_hour_reserves_count WHERE restaurant_id = :id AND `date` = :date AND `time` = :time'), ['id' => $sch->restaurant_id, 'date' => $date_str, 'time' => $hour_str]);

                        $reserve_count = 0;
                        if(count($rsh) > 0)
                            $reserve_count = $rsh[0]->pending_count;
                        else{
                            $hour_start = $date_str.' '.($date->copy()->subMinutes($sch->average_time - 30)->format('H:i:s')); // Hora de inicio a buscar
                            $hour_end = $date->toDateTimeString(); // Hora de fin a buscar

                            $reserve_count = UserReserve::where(['restaurant_id' => $sch->restaurant_id])->where('date','>=', $hour_start)->where('date', '<', $hour_end)->whereNull('date_end')->sum('num_pers');
                        }

                        $isFull = ($sch->capacity < $reserve_count+$num_pers);
                    }

                    return $isOpen && !$isFull;
                })->pluck('restaurant_id'));

            }else return response()->json($results);
        }

        if(isset($data['q'])){
            $restaurant_ids = (clone $restaurants)->pluck('id');

            $menu_items = RestaurantMenuItem::whereIn('restaurant_menu_section_id', RestaurantMenuSection::whereIn('restaurant_id', $restaurant_ids)->pluck('id'))->search("*$data[q]*")->take(5)->get();

            $results['eat'] = MenuItemResource::collection($menu_items);
            
            $restaurants->search("*$data[q]*");
        }
        
        $results['restaurants']['pages'] = ceil($restaurants->count()/$results_x_page);

        if(!isset($data['q']))
            $restaurants->leftJoinRelations(['reviews', 'reserves'])->select(array('restaurant.*',
                DB::raw('AVG(restaurant_review.rate) as ratings_average'),
                DB::raw('COUNT(user_reserve.date_end) as reserves_count')
            ))->groupBy('id')->orderBy('ratings_average', 'DESC')->orderBy('reserves_count', 'DESC');

        $restaurants->forPage($num_page, $results_x_page);

        $restaurants_json = $restaurants->get();

        $results['restaurants']['data'] = RestaurantCardResource::collection($restaurants_json);

        return response()->json($results);
    }
}