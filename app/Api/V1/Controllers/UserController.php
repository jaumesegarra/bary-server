<?php

namespace App\Api\V1\Controllers;

use App\Http\Controllers\Controller;

use Auth;
use App\Api\V1\Requests\AdditionalFieldsRequest;
use App\Notifications\Verify;
use App\Api\V1\Resources\RestaurantCardResource;
use App\Api\V1\Resources\ReserveResource;

class UserController extends Controller
{

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {   
        $this->middleware('auth:api');
    }

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function info()
    {   
        $user = Auth::guard()->user();

        $response = ['name' => $user->name, 'surname' => $user->surname, 'confirmed_email' => $user->is_verified];

        $current_time = time();
        $ttl = Auth::guard()->factory()->getTTL();

        if($ttl > $current_time && $current_time <= $ttl/2)
            $response['token'] = Auth::guard()->refresh();

        return response()->json($response);
    }

    /**
     * Get the information about User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function profile()
    {   
        $user = Auth::guard()->user();

        $response = [
            'name' => $user->name, 
            'surname' => $user->surname,
            'email' => $user->email,
            'favourites' => RestaurantCardResource::collection($user->favourites()->take(5)->get()),
            'reserves' => ReserveResource::collection($user->reserves()->take(5)->get())
        ];

        return response()->json($response);
    }

    /**
     * Save required additional fields for reserve
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function resend()
    {
        $status = 0;
        $user = Auth::guard()->user();

        if(!$user->is_verified){
            $user->verification_code = str_random(30);
            if($user->save()){
                $user->notify(new Verify($user->name, $user->verification_code));

                $status = 1;
            }
        }else $status = 1;

        return response()->json(['status' => $status]);
    }


    /**
     * Get required additional fields for reserve
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function required_additional_fields()
    {
        $fields = [];
        $user = Auth::guard()->user();

        if(!isset($user->dni))
            $fields[] = 'dni';

        if(!isset($user->phone))
            $fields[] = 'phone';

        return $fields;
    }

    /**
     * Save required additional fields for reserve
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function post_required_additional_fields(AdditionalFieldsRequest $request)
    {
        $data = $request->only(['dni', 'phone']);
        $user = Auth::guard()->user();

        if($user->update($data))
            return response()->json(['status' => 1]);
    }
}
