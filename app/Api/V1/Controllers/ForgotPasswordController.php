<?php

namespace App\Api\V1\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\ForgotPasswordRequest;

class ForgotPasswordController extends Controller
{
    public function sendResetEmail(ForgotPasswordRequest $request)
    {   
        $user = User::where('email', $request->email)->first();

        if(!$user)
            return response()->json(['status' => -1], 422);

        try {
            $user->sendPasswordResetNotification($user->createResetPasswordToken());
        } catch (\Exception $e) {
            //Return with error
            $error_message = $e->getMessage();
            return response()->json(['status' => 0, 'error' => $error_message], 500);
        }

        return response()->json([
            'status' => 1
        ],200);
    }
}
