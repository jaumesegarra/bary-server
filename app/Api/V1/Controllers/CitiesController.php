<?php

namespace App\Api\V1\Controllers;

use App\Http\Controllers\Controller;

use App\City;
use App\Restaurant;
use App\Api\V1\Resources\CityResource;

class CitiesController extends Controller
{

    /**
     * Create a new instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Get all cities
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index() {
        $cities = City::whereIn('id', Restaurant::where('approbed', '=', 1)->groupBy('city_id')->pluck('city_id'))->get();

        return response()
        ->json([
            'cities' => CityResource::collection($cities)
        ]);
    }
}