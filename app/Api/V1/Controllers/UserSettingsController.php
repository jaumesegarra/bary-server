<?php

namespace App\Api\V1\Controllers;

use App\Http\Controllers\Controller;

use Auth;
use App\Api\V1\Resources\UserResource;
use App\Api\V1\Requests\UserDataRequest;
use App\Api\V1\Requests\UserChangeEmailRequest;
use App\Api\V1\Requests\UserChangePasswordRequest;
use App\Api\V1\Requests\DeleteAccountRequest;

use Carbon\Carbon;
use App\Notifications\EmailChanged;

class UserSettingsController extends Controller
{

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {   
    	$this->middleware('auth:api');
    }

    /**
     * Get data of the user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {      
    	$user = Auth::guard()->user();

    	return response()->json(new UserResource($user));
    }

    /**
     * Save data of the user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(UserDataRequest $request)
    {     
        $data = $request->only(['name', 'surname', 'genre', 'dni', 'phone']);

        $user = Auth::guard()->user();

        $user->name = $data['name'];
        $user->surname = $data['surname'];
        $user->genre = $data['genre'];

        $user->dni = (isset($data['dni']) ? $data['dni'] : null);
        $user->phone = (isset($data['phone']) ? $data['phone'] : null);

        $result = $user->save();

        return response()->json(['status' => ($result ? 1: 0)], ($result ? 200 : 500));
    }

    /**
     * Change email of the user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function change_email(UserChangeEmailRequest $request)
    {     
        $data = $request->only(['email', 'password']);

        $user = Auth::guard()->user();
        $reset_code = encrypt([
            'id' => $user->id,
            'email' => $user->email,
            'date_end' => Carbon::today()->addDays(2)->toDateString()
        ]);

        try{
            $user->notify(new EmailChanged($reset_code, $data['email']));
        }catch(\Exception $exception){}

        $user->email = $data['email'];
        $user->is_verified = 0;

        // Deberia reenviar correo de verificación
        
        $result = $user->save();

        return response()->json(['status' => ($result ? 1: 0)], ($result ? 200 : 500));
    }

    /**
     * Change password of the user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function change_password(UserChangePasswordRequest $request)
    {     
        $data = $request->only(['password']);

        $user = Auth::guard()->user();
        $user->password = $data['password'];

        $result = $user->save();

        return response()->json(['status' => ($result ? 1: 0)], ($result ? 200 : 500));
    }

    /**
     * Delete account of the user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete_account(DeleteAccountRequest $request)
    {

        $user = Auth::guard()->user();

        $result = $user->delete();

        return response()->json(['status' => ($result ? 1: 0)], ($result ? 200 : 500));
    }
}
