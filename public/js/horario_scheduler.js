$.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
});

var EDITABLE = $('#organizeScheduler').hasClass('editable');

$('#organizeScheduler').fullCalendar({
	schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
	now:moment(),
	header:false,
	defaultView: 'agendaDay',
	editable:EDITABLE,
	selectable:EDITABLE,
	eventOverlap: false,
	selectOverlap:false,
	allDaySlot: false,
	timeFormat: 'H:mm',
	slotLabelFormat: 'H',
	select: function (start, end, jsEvent, view, resource) {
		create_day(resource.id, start, end);
	},
	eventRender: function(event, element) {
		if(EDITABLE){
			var $closeA = $("<span class='closeon'><i class='la la-close'></i></span>");
			$closeA.click(function() {
				vex.dialog.confirm({
					message: '¿Está seguro que desea eliminar este horario?',
					callback: function (value) {
						if (value) {
							remove_day(event.id);
						}
					}
				});
			});

			element.append($closeA);
		}
	},
	eventDrop: function(event, delta, revertFunc) {
		if(event.end.date() != event.start.date())
			revertFunc();
		else
			update_day(event.id, event.resourceId, event.start, event.end, revertFunc);
	},
	eventResize: function(event, delta, revertFunc) {
		update_day(event.id, event.resourceId, event.start, event.end, revertFunc);
	},
	resourceLabelText: 'Dias',
	resources: [
	{
		id: 'L',
		title: 'Lunes',
		eventColor: '#fc9292',
		eventTextColor: '#e10d0d'
	},
	{
		id: 'M',
		title: 'Martes',
		eventColor: '#cdb8fc',
		eventTextColor: '#7a44f2'
	},
	{
		id: 'X',
		title: 'Miercoles',
		eventColor: '#fcd7f8',
		eventTextColor: '#EF56E5'
	},
	{
		id: 'J',
		title: 'Jueves',
		eventColor: '#CCF3F1',
		eventTextColor: '#3FC6BE'
	},
	{
		id: 'V',
		title: 'Viernes',
		eventColor: '#fec698',
		eventTextColor: '#fc790e'
	},
	{
		id: 'S',
		title: 'Sabado',
		eventColor: '#A7F5C6',
		eventTextColor: '#2DBC66'
	},
	{
		id: 'D',
		title: 'Domingo',
		eventColor: '#b2d5fc',
		eventTextColor: '#509ff8'
	}
	]
});

$('.fc-scroller').scrollTop($('.fc-scroller').height()+$('.fc-scroller').offset().top);