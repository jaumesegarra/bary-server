$.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
});

var EDITABLE = $('#reservesScheduler').hasClass('editable');
var showFinalized = false;
var showCanceled = false;

$('#reservesScheduler').fullCalendar({
	customButtons: {
		goDateButton: {
			text: 'Ir a fecha',
			click: function() {
				vex.dialog.prompt({
					message: 'Introduce la fecha:',
					placeholder: 'dd/mm/yyyy',
					callback: function (value) {
						if(value && moment(value, "DD/MM/YYYY").isValid())
							$('#reservesScheduler').fullCalendar('gotoDate', moment(value, "DD/MM/YYYY").toDate());
					}
				});
				$('.vex-content .vex-dialog-input input.vex-dialog-prompt-input').datepicker({
					beforeShow: function() {
						$(this).datepicker("widget").wrap('<div class="ll-skin-melon"></div>');
					},
					onClose: function() {
						$(this).datepicker("widget").hide();
						$(this).datepicker("widget").unwrap();
					}
				});
				$('.vex-content .vex-dialog-buttons .vex-dialog-button-secondary').focus();
			}
		}
	},
	buttonText: {
		today: 'Hoy',
	},
	header: {
		left: 'title',
		center: '',
		right: 'today goDateButton prev,next'
	},
	defaultView: 'listDay',
	now:moment.utc(moment().format('YYYY-MM-DD HH:mm')),
	timeFormat: 'H:mm',
	allDaySlot: false,
	locale: 'es',
	events: GET_EVENTS_URL,
	eventRender: function(event, element) {

		element[0].setAttribute('data-code', event.id);

		element.qtip({
			content: {
				text: ((event.description) ? '<div class="suggestions">'+event.description+'</div>' : '')+'<div class="num_pers">Para '+event.num_pers+' personas</div>'+((event.end) ? '<div class="reserve_finished">Estacia finalizada a las '+moment(event.end).format('H:mm')+'</div>' : ''),
				title: event.title+'<span class="time">'+moment(event.start).format('H:mm')+'</span>',
			},
			position: {
				at: 'bottom left'
			},
			style: {
				classes: 'qtip-light qtip-shadow'
			}
		});

		$('.fc-list-item-title', element).append(' <span class="num_pers">'+event.num_pers+' pers.</span>');
		$(element).append('<td class="fc-list-item-buttons"></td>');

		if(event.canceled){
			$('.fc-list-item-marker .fc-event-dot', element).css('background-color', 'red');
			element[0].classList.add('canceled');
		}else if(event.end){
			$('.fc-list-item-marker .fc-event-dot', element).css('background-color', '#ddd');
			element[0].classList.add('finalized');
			$('.fc-list-item-time', element).text(moment(event.start).format('H:mm'));
		}else{
			if(EDITABLE){
				if(moment.utc(moment().format('YYYY-MM-DD HH:mm')).isSameOrAfter(event.start))
					$('.fc-list-item-buttons', element).append('<button class="button small white finalize_reserve" title="Finalizar reserva">Finalizar</button>');
				if(moment.utc(moment().format('YYYY-MM-DD HH:mm')).isSameOrBefore(event.start))
					$('.fc-list-item-buttons', element).append('<a href="#" class="cancel_reserve" title="Cancelar Reserva"><i title="" class="la la-ban"></i></a>');
			}
		}
	},
	eventAfterAllRender: function(){
		$('.fc-list-table .fc-list-heading td').attr('colspan', 4);

		var $labelShowFinalized = $('<label title="Finalizadas">F</label>');
		var $inputShowFinalized = $('<input type="checkbox" />');
		if(showFinalized)
			$inputShowFinalized.attr('checked', 'checked');
		$inputShowFinalized.on('change', function(e){
			e.preventDefault();
			if($(this).is(':checked'))
				$('.fc-list-table').addClass('show-finalized');
			else
				$('.fc-list-table').removeClass('show-finalized');

			showFinalized = $(this).is(':checked');
		});
		$labelShowFinalized.prepend($inputShowFinalized);
		$('.fc-widget-header').append($labelShowFinalized);

		var $labelShowCanceled = $('<label title="Canceladas">C</label>');
		var $inputShowCanceled = $('<input type="checkbox" />');
		if(showCanceled)
			$inputShowCanceled.attr('checked', 'checked');
		$inputShowCanceled.on('change', function(e){
			e.preventDefault();
			if($(this).is(':checked'))
				$('.fc-list-table').addClass('show-canceled');
			else
				$('.fc-list-table').removeClass('show-canceled');

			showCanceled = $(this).is(':checked');
		});
		$labelShowCanceled.prepend($inputShowCanceled);
		$('.fc-widget-header').append($labelShowCanceled);

		if(showFinalized) $('.fc-list-table').addClass('show-finalized');
		if(showCanceled) $('.fc-list-table').addClass('show-canceled');

	},
	loading: function (bool) { 
		if (bool) 
			loader_bar(true);
		else 
			loader_bar(false);
	}
});

var $refreshBtn = $('<button class="fc-button fc-state-default fc-corner-left fc-corner-right" title="Recargar"><i class="la la-refresh"></i></button>');
$refreshBtn.on('click', function(e){
	e.preventDefault();
	$('#reservesScheduler').fullCalendar('refetchEvents');
});
$('.fc-right > .fc-button-group').before($refreshBtn);

if(EDITABLE){
	var $createBtn = $('<button class="fc-button fc-state-default fc-corner-left fc-corner-right" title="Crear"><i class="la la-plus"></i></button>');
	$createBtn.on('click', function(e){
		e.preventDefault();

		vex.dialog.open({
			message: 'Crear nueva reserva',
			input: [
			'<style>',
			'.vex-custom-field-wrapper {',
			'margin: 5px 0;',
			'}',
			'.vex-custom-field-wrapper > label {',
			'display: inline-block;',
			'margin-bottom: .2em;',
			'}',
			'.vex.vex-theme-top .vex-dialog-form .vex-dialog-input select {',
			'min-height: 0;',
			'padding: 10px .67em;',
			'}',
			'.reserve_available_after {',
			'font-size: 14px;',
			'color: #e41010;',
			'}',
			'.vex.vex-theme-top .vex-dialog-form .vex-dialog-input textarea {',
			'padding: 10px;',
			'max-width: 100%',
			'min-width: 100%',
			'}',
			'</style>',
			'<div class="vex-custom-field-wrapper">',
			'<label for="name">Nombre</label>',
			'<div class="vex-custom-input-wrapper">',
			'<input name="name" type="text" placeholder="Nombre"/>',
			'</div>',
			'</div>',
			'<div class="vex-custom-field-wrapper">',
			'<label for="suggestions">Sugerencias</label>',
			'<div class="vex-custom-input-wrapper">',
			'<textarea name="suggestions" maxlength="255" placeholder="Sugerencias"></textarea>',
			'</div>',
			'</div>',
			'<div class="vex-custom-field-wrapper">',
			'<label for="date">Dia</label>',
			'<div class="vex-custom-input-wrapper">',
			'<input name="date" type="text" placeholder="dd/mm/yyyy"/>',
			'</div>',
			'</div>',
			'<div class="vex-custom-field-wrapper">',
			'<label for="hour">Hora</label>',
			'<div class="vex-custom-input-wrapper">',
			'<select name="hour"><option value="" selected="selected" disabled="disabled">---</option></select>',
			'</div>',
			'<div class="vex-custom-field-wrapper">',
			'<label for="num_pers">Número de personas</label>',
			'<div class="vex-custom-input-wrapper">',
			'<select name="num_pers">',
			'<option value="1">1 pers.</option>',
			'<option value="2" selected="selected">2 pers.</option>',
			'<option value="3">3 pers.</option>',
			'<option value="4">4 pers.</option>',
			'<option value="5">5 pers.</option>',
			'<option value="5">5 pers.</option>',
			'<option value="6">6 pers.</option>',
			'<option value="7">7 pers.</option>',
			'<option value="8">8 pers.</option>',
			'<option value="9">9 pers.</option>',
			'<option value="10">10 pers.</option>',
			'<option value="11">11 pers.</option>',
			'<option value="12">12 pers.</option>',
			'<option value="13">13 pers.</option>',
			'<option value="14">14 pers.</option>',
			'<option value="15">15 pers.</option>',
			'</select>',
			'</div>',
			'<div class="reserve_available_after" style="display:none">',
			'Se superará el aforo máximo en ',
			'<b></b> ',
			'personas',
			'</div>',
			'</div>'
			].join(''),
			callback: function (data) {
				if (data && data.name && data.date && moment(data.date, "DD/MM/YYYY").isValid() && data.hour && data.num_pers) {
					loader_bar(true);
					$.ajax({
						type: "POST",
						url: CREATE_URL,
						data: data
					}).done(function(data) {
						$('#reservesScheduler').fullCalendar('refetchEvents');
					}).fail(function(jqXHR, textStatus) {
						console.error(textStatus, jqXHR);
					}).then(function(){
						loader_bar(false);
					});
				}
			}
		});

		$('.vex-content .vex-dialog-input input[name="date"]').datepicker({
			dateFormat: 'dd/mm/yy',
			minDate: new Date(),
			beforeShow: function() {
				$(this).datepicker("widget").wrap('<div class="ll-skin-melon"></div>');
			},
			onClose: function() {
				$(this).datepicker("widget").hide();
				$(this).datepicker("widget").unwrap();
			}
		});

		var capacity_hours;
		function get_hours(date) {
			if(date && moment(date, "DD/MM/YYYY").isValid()){
				$('.vex-content .vex-dialog-input input[name="date"]').attr('disabled', 'disabled');
				$('.vex-content .vex-dialog-input select[name="hour"]').html('<option value="" selected="selected" disabled="disabled">---</option>');
				get_available_reserves_after();

				$.ajax({
					type: "GET",
					url: GET_AVHOURS_OF_DAY,
					data: {
						'date': date
					}
				}).done(function(data) {
					capacity_hours = [];
					for (var i = 0; i < data.hours.length; i++) {
						var hour = data.hours[i];
						$('.vex-content .vex-dialog-input select[name="hour"]').append('<option value="'+hour.hour+'">'+hour.hour+'</option>');
						capacity_hours[hour.hour] = hour.num_pers;
					}
					console.log(capacity_hours);
				}).fail(function(jqXHR, textStatus) {
					console.error(textStatus, jqXHR);
				}).then(function(){
					$('.vex-content .vex-dialog-input input[name="date"]').removeAttr('disabled');
				});
			}
		};

		$('.vex-content .vex-dialog-input input[name="date"]').on('change', function(){
			var date = $(this).val();
			get_hours(date);
		});

		function get_available_reserves_after(){
			var $div = $('.reserve_available_after');
			var hour = $('.vex-content .vex-dialog-input select[name="hour"]').val();
			var num_pers = $('.vex-content .vex-dialog-input select[name="num_pers"]').val();

			if(hour != "" && capacity_hours != undefined){
				var capacity = capacity_hours[hour]-num_pers;

				if(capacity < 0){
					$('b', $div).text(capacity*-1);
					$div.show();
				}
			}else $div.hide();
		}

		$('.vex-content .vex-dialog-input select[name="hour"]').on('change', function(){
			get_available_reserves_after();
		});

		$('.vex-content .vex-dialog-input select[name="num_pers"]').on('change', function(){
			get_available_reserves_after();
		});
	});
$('.fc-right').prepend($createBtn);

$('#reservesScheduler').on('click', '.cancel_reserve', function(e){
	e.preventDefault();

	var code = $(this).parents('.fc-list-item').data('code');
	vex.dialog.confirm({
		message: '¿Está seguro que desea cancelar esta reserva?',
		callback: function (value) {
			if (value) {
				loader_bar(true);
				$.ajax({
					type: "POST",
					url: CANCEL_URL.replace('_i_', code),
				}).done(function(msg) {
					$('#reservesScheduler').fullCalendar('refetchEvents');
				}).fail(function(jqXHR, textStatus) {
					console.error(textStatus, jqXHR);
				}).then(function(){
					loader_bar(false);
				});
			}
		}
	})
});

$('#reservesScheduler').on('click', '.finalize_reserve', function(e){
	e.preventDefault();
	var code = $(this).parents('.fc-list-item').data('code');

	vex.dialog.confirm({
		message: '¿Está seguro que desea marcar como finalizada la estancia de esta reserva?',
		callback: function (value) {
			if (value) {
				loader_bar(true);
				$.ajax({
					type: "POST",
					url: FINALIZE_URL.replace('_i_', code),
				}).done(function(msg) {
					$('#reservesScheduler').fullCalendar('refetchEvents');
				}).fail(function(jqXHR, textStatus) {
					console.error(textStatus, jqXHR);
				}).then(function(){
					loader_bar(false);
				});
			}
		}
	})
});
}