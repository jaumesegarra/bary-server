var dataMonths = {
	datasets: MONTH_RESERVES,
	labels: [
	'Ene',
	'Feb',
	'Mar',
	'Abr',
	'May',
	'Jun',
	'Jul',
	'Ago',
	'Sep',
	'Oct',
	'Nov',
	'Dic'
	]
};

var lineOptions = {
	responsive: true,
	elements: {
		line: {
			tension: 0,
		}
	},
	scales: {
		yAxes: [{
			ticks: {
				beginAtZero:true
			}
		}]
	}
};
var reserve_months_line = new Chart(document.querySelector('.reserve_months canvas'), {
	type: 'line',
	data: dataMonths,
	options: lineOptions
});

var myBarChart = new Chart(document.querySelector('.week_average_time canvas'), {
	type: 'bar',
	data: {
		datasets: [{
			data: $('.week_average_time canvas').data('value'),
			backgroundColor: ['#ffe0e6', '#ffecd9', '#fff5dd', '#dbf2f2', '#d7ecfb', '#ebe0ff', '#f4f5f5'],
			borderColor: ['#ff7592', '#ffaa56', '#ffd36a', '#60c7c7', '#4eaded', '#a578ff', '#cfd1d4'],
			borderWidth: 2
		}],

		labels: [
		'Lunes',
		'Martes',
		'Miercoles',
		'Jueves',
		'Viernes',
		'Sabado',
		'Domingo'
		]
	},
	options: {
		legend: {
			display: false
		},
		scales: {
			yAxes: [{
				ticks: {
					beginAtZero:true
				}
			}]
		},
		responsive: true
	}
});

var myRadarChart = new Chart(document.querySelector('.reserve_types canvas'), {
    type: 'pie',
    data: { 
    	datasets: [{
			data: $('.reserve_types canvas').data('value'),
			backgroundColor: ['#dbf2f2', '#ffe0e6', '#fff5dd'],
			borderColor: ['#60c7c7', '#ff7592', '#ffd36a']
		}],

		labels: [
		'Finalizadas',
		'Canceladas',
		'No finalizadas'
		],	
	},
    options: {
		legend: {
			display: false
		},
		responsive: true
	}
});
