$('.delete_js').on('click', function(e){
	e.preventDefault();

	var $card = $(this).parents('.card');
	var $delete_form = $('.destroy_sph', $card);

	vex.dialog.confirm({
		message: "¿Está seguro/a que desea eliminar esta imagen?",
		callback: function (value) {
			if (value){
				$delete_form.submit();
			}
		}
	});
});