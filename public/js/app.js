$.widget("custom.combobox", {
	_create: function() {
		this.wrapper = $("<div>")
		.addClass("custom-combobox")
		.insertAfter(this.element);

		this.element.hide();
		this._createAutocomplete();
		this._createShowAllButton();
	},

	_createAutocomplete: function() {
		var selected = (this.element.attr('value') != "") ? this.element.children("[value='"+this.element.attr('value')+"']") : this.element.children(":selected"),
		value = selected.val() ? selected.text() : "";

		this.input = $("<input type='text' value=''>")
		.appendTo(this.wrapper)
		.val(value)
		.attr("title", "")
		.autocomplete({
			delay: 0,
			minLength: 0,
			source: $.proxy(this, "_source")
		});

		this._on(this.input, {
			autocompleteselect: function( event, ui ) {
				ui.item.option.selected = true;
				this._trigger( "select", event, {
					item: ui.item.option
				});
			},

			autocompletechange: "_removeIfInvalid"
		});
	},

	_createShowAllButton: function() {
		var input = this.input,
		wasOpen = false;

		$("<a><i class='la la-caret-down'></i></a>")
		.attr("tabIndex", -1 )
		.appendTo(this.wrapper)
		.on("click", function() {
			input.trigger( "focus");

			// Close if already visible
			if ( wasOpen ) {
				return;
			}

			// Pass empty string as value to search for, displaying all results
			input.autocomplete("search", "");
		});
	},

	_source: function( request, response ) {
		var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
		response( this.element.children("option").map(function() {
			var text = $(this).text();

			if (this.value && (!request.term || matcher.test(text)))
				return {
					label: text,
					value: text,
					option: this
				};
			}));
	},
	_removeIfInvalid: function( event, ui ) {
		// Selected an item, nothing to do
		if ( ui.item ) {
			return;
		}

		// Search for a match (case-insensitive)
		var value = this.input.val(),
		valueLowerCase = value.toLowerCase(),
		valid = false;
		this.element.children("option").each(function() {
			if ( $( this ).text().toLowerCase() === valueLowerCase ) {
				this.selected = valid = true;
				return false;
			}
		});

		// Found a match, nothing to do
		if (valid) {
			return;
		}

		// Remove invalid value
		this.input.val("");
		this.element.val("");

		this._delay(function() {
			this.input.tooltip("close").attr( "title", "" );
		},2500);

		this.input.autocomplete("instance").term = "";
	},

	_destroy: function() {
		this.wrapper.remove();
		this.element.show();
	}
});

$('body').on('click', '.dropdown-menu > a', function(e){
	e.preventDefault();

	var $ul = $(this).parent().children('ul');

	function e_close(e){		
		if($(e.relatedTarget).parents('.dropdown-menu').length > 0)
			e.relatedTarget.click();

		close();
	}

	function close(){
		$ul.hide();
		$(this).off('blur', e_close);
	}

	if(!$ul.is(':visible')){
		$ul.show();
		$(this).on('blur', e_close);
	}else close();
});

$('body').on('click', '.logout_js > a', function(e){
	e.preventDefault();
	
	$(this).parent().submit();
});

$('.content-panel > .nav > .showMenu').on('click', function(e){
	e.preventDefault();

	$nav = $(this).parent();

	if($('i', this).hasClass('la-bars')){
		$('i', this).removeClass('la-bars').addClass('la-close');
	}else{
		$('i', this).removeClass('la-close').addClass('la-bars');
	}

	$nav.toggleClass('show');
});

$('body').on('click', 'table tr[href]', function(e){
	e.preventDefault();
	var href = $(this).attr('href');
	document.location.href= href;
});

$.datepicker.setDefaults($.datepicker.regional["es"]);

function loader_bar(show){
	var $bar = $('span.loading-bar');
	if (show)
		$bar.addClass('show');
	else
		$bar.removeClass('show');
}

$('.aprove_request_js').on('click', function(e){
	var $form = $('.aprove_request_form_js', $(this).parent());

	vex.dialog.confirm({
		message: '¿Está seguro que solicitar la aprobación del restaurante? No podrá modificar los datos personales de este hasta su aprobación.',
		callback: function (value) {
			if (value) {
				$form.submit();
			}
		}
	});
});